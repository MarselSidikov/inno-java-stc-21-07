/**
 * 03.06.2021
 * 11. Human Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HumansStatistic {
    public int[] calcAges(Human[] humans, int maxAge) {
        int ages[] = new int[maxAge];

        // сортировка подсчетом
        // используется когда диапазон значений ограничен (0 .. 120)
        for (int i = 0; i < humans.length; i++) {
            // получаю возраст текущего человека
            // например, человек имеет возраст 75
            int currentAge = humans[i].getAge();
            // возраст человека беру в качестве индекса массива ages и под этим индексом увеличиваю значение на единицу
            // ages[75]++, следовательно, 75-й элемент имеет значение на 1 больше того, которое было до этого
            ages[currentAge]++;
        }

        return ages;
    }
}
