import java.util.Random;

/**
 * 03.06.2021
 * 11. Human Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class HumansGenerator {
    public String randomName() {
        Random random = new Random();
        // массив символов
        char name[] = new char[10];
        for (int i = 0; i < name.length; i++) {
            // генерируем код символа и кладем в массив
            name[i] = (char) (random.nextInt(92 - 65) + 65);
        }
        // создаем строку на основе массива символов
        return new String(name);
    }

    public Human[] generateHumans(int humansCount, int maxAge) {
        Random random = new Random();
        Human humans[] = new Human[humansCount];

        for (int i = 0; i < humans.length - 1; i++) {
            // создали человека, дали ему случайное имя, дали ему случайны возраст
            Human newHuman = new Human(randomName(), random.nextInt(maxAge));
            // положили человека в массив
            humans[i] = newHuman;
        }

        Human defaultHuman = new Human();
        humans[humansCount - 1] = defaultHuman;

        return humans;
    }
}
