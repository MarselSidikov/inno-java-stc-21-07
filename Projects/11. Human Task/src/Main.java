import java.util.Random;

public class Main {

    public static void main(String[] args) {
        HumansGenerator generator = new HumansGenerator();
        HumansStatistic statistic = new HumansStatistic();
        Printer printer = new Printer();
        // сгенерировали людей
        Human[] humans = generator.generateHumans(100, 120);
        // почему это проблема?
        // ages[humans[i].age]++ -> ages[humans[3].age]++ -> ages[-10]++ -> EXCEPTION
        // humans[3].age = -10;
        humans[3].setAge(-10);
        // посчитали возраста
        int[] ages = statistic.calcAges(humans, 120);
        // распечатали людей
        printer.printHumans(humans);

        System.out.println("--------");
        // распечатали возраста
        printer.printAges(ages);
    }
}
