/**
 * 03.06.2021
 * 11. Human Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// инкапсуляция
// -> состояние и поведение в одной сущности - в объекте
// -> защита состояния с помощью МОДИФИКАТОРОВ ДОСТУПА И МЕТОДОВ ДОСТУПА (геттеры и сеттеры)
// -> this - ключевое слово, которое позволяет обращаться к членам класса
// -> this - объектная переменная, которая ссылается на объект из которого происходит вызов метода
public class Human {
    String name;
    private int age;

    // конструктор позволяет выполнить инициализацию объекта, т.е. дать ему начальные характеристики (значения полям)
    public Human() {
        name = "DEFAULT";
        age = 0;
    }

    public Human(String name, int age) {
        this.name = name;
        //age = Age;
        setAge(age);
    }

    // метод доступа - геттер
    // нужен для получения значения поля
    public int getAge() {
        return age;
    }

    // метод доступа - сеттер
    // нужен для того, чтобы класть значение в поле
    // но контролируемо
    public void setAge(int age) {
        if (age >= 0) {
            this.age = age;
        } else {
            this.age = 0;
        }
    }
}
