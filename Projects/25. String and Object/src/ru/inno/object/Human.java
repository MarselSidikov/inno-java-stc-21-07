package ru.inno.object;

import java.util.Objects;

/**
 * 24.06.2021
 * 25. String and Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human {
    private String name;
    private int age;
    private boolean isWorker;

    public Human(String name, int age, boolean isWorker) {
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    @Override
    public String toString() {
        return "Human(name = " + name + ", age = " + age + ", isWorker = " + isWorker + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return age == human.age &&
                isWorker == human.isWorker &&
                Objects.equals(name, human.name);
    }

    // true - если объекты равны
    // false - если не равны
//    @Override
//    public boolean equals(Object object) {
////        if (object == null) {
////            return false;
////        }
//
//        // убедиться, что в Object находится объект такого же типа, какого типа наш класс
//        // если этот Object не экземпляр Human
//        if (!(object instanceof Human)) {
//            return false;
//        }
//
//        if (this == object) {
//            return true;
//        }
//
//        // здесь мы знаем, что object - экземпляр Human, он не null и это не тот же самый объект который сравниваем
//        // следовательно, можем сделать явное понижающее преобразование гарантированно без ошибок
//
//        Human that = (Human)object;
//
//        return this.age == that.age
//                && this.isWorker == that.isWorker
//                && this.name.equals(that.name);
//    }

//
//    public boolean equals(Human that) {
//        if (that == null) {
//            return false;
//        }
//
//        if (this == that) {
//            return true;
//        }
//
//        return this.name.equals(that.name) && this.isWorker == that.isWorker && this.age == that.age;
//    }
}
