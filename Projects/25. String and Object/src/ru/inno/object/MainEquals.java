package ru.inno.object;

/**
 * 24.06.2021
 * 25. String and Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainEquals {

    public static boolean allEquals(Object ... objects) {
        for (int i = 0; i < objects.length - 1; i++) {
            if (!objects[i].equals(objects[i+1])) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Human marsel = new Human("Марсель", 27, true);
        Human marsel1 = new Human("Марсель", 27, true);
        Human marsel2 = new Human("Марсель", 27, true);
        Human marsel3 = new Human("Марсель", 27, true);
//        System.out.println(allEquals("Hello", "Hello", "Hello", "Hello", "Hello"));
        System.out.println(allEquals(marsel, marsel1, marsel2, marsel3));

    }
}
