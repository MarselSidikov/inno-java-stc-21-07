package ru.inno.string;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * 24.06.2021
 * 25. String and Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

// https://www.javatpoint.com/why-string-is-immutable-or-final-in-java#:~:text=The%20String%20is%20immutable%20in,it%20makes%20the%20String%20immutable.
public class MainStringIntro {
    public static void main(String[] args) {
        // final-класс - нельзя наследоваться
        // String - содержит поле final char value[]; -> массив символов
        String h1 = "Hello";
        String h2 = h1.replace("e", "u");
        System.out.println(h2);
    }
}
