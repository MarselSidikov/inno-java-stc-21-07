package ru.inno.string;

/**
 * 24.06.2021
 * 25. String and Object
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainStringEquals {
    public static void main(String[] args) {
        // java все литеральные (т.е. прям в коде) строки помещает в String Pool
        String s1 = "java";
        String s2 = "java";
        // явно создали строку, т.е. выделили для нее отдельный объект
        String s3 = new String("java");
//        System.out.println(s1.equals(s2));
        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
        System.out.println(s3.intern() == s2);
        System.out.println(s3 == s1);
    }
}
