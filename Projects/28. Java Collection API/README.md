* `Iterable` - интерфейс, который показывает, что коллекция может быть "итерируема". Т.е. для нее можно 
попросить итератор.

```java
public interface Iterable<T> {
    Iterator<T> iterator();
}
```

* `Iterator` - интерфейс, объекты которого имеют функциональность для последовательного обхода 
чего-либо (например коллекций).

```java
public interface Iterator<E> {
    boolean hasNext();
    
    E next();
}
```

* `Collection` - интерфейс, описывающий какую-либо коллекцию.

```java
public interface Collection<E> extends Iterable<E> {
    int size();
    
    boolean isEmpty();
    
    boolean contains(Object o);
    
     void clear();

    boolean add(E e);
    
    boolean remove(Object o);
}
```

* `List` - интерфейс, описывающий коллекцию типа "список". Гарантирует порядок элементов.

```java
public interface List<E> extends Collection<E> { 
    E set(int index, E element);
    
    void add(int index, E element);
    
    E remove(int index);
    
    int indexOf(Object o);

    int lastIndexOf(Object o);
    
    E get(int index);
}
```

* `ArrayList` - реализация списка на основе массива.

```java
public class ArrayList<E> implements List<E> {
    // размер массива по умолчанию
    private static final int DEFAULT_CAPACITY = 10;
    // массив, который содержит необходимые элементы
    transient Object[] elementData;
    // количество элементов
    private int size;
    
    public boolean add(E e) {
        // обеспечить в массиве размер, позволяющий добавить очередной элемент
        ensureCapacityInternal(size + 1);
        elementData[size] = e;
        size++;
        return true;
    }

    private void ensureCapacityInternal(int minCapacity) {
        // проверяем, какой объем нужно выделить?
        // мы в принципе не можем сделать объем меньше 10
        // если например необходимо добавить 11 элемент, то размер массива будет увелчиен до 15
        // если меньше, то все равно будет 10
        ensureExplicitCapacity(calculateCapacity(elementData, minCapacity));
    }
    private void ensureExplicitCapacity(int minCapacity) {
        // если требуемый размер больше, чем то, что есть у нас сейчас
        if (minCapacity - elementData.length > 0)
            grow(minCapacity);
    }

    private void grow(int minCapacity) {
        // запоминаем старый размер
        int oldCapacity = elementData.length;
        // считаем новый - к старому прибавляем его половину - увеличиваем в полтора раза
        int newCapacity = oldCapacity + (oldCapacity >> 1); // то же самое, что поделить на 1/2
        // если вышло так, что новый размер меньше требуемого
        if (newCapacity - minCapacity < 0)
            // требуемый размер и есть новый
            newCapacity = minCapacity;
        // если требуемый размер больше, чем то, что мы по факту можем позволить
        if (newCapacity - MAX_ARRAY_SIZE > 0)
            // оставляем максимально возможный
            newCapacity = hugeCapacity(minCapacity);
         // создаем копию старого массива, но большего размера
         elementData = Arrays.copyOf(elementData, newCapacity); // для копирования вызывается System.arraycopy
    }
}
```

* `LinkedList` - реализация списка на основе двусвязных узлов.

```java
public class LinkedList<E> implements List<E> {
    int size = 0;
    // первый узел
    transient Node<E> first;
    // последний узел
    transient Node<E> last;
    
    private static class Node<E> {
            E item;
            // ссылка на следующий элемент
            Node<E> next;
            // ссылка на предыдущий элемент
            Node<E> prev;
    }

    public E get(int index) {
        checkElementIndex(index);
        return node(index).item;
    }
    
    Node<E> node(int index) {
        
        // если индекс находится в левой части списка
        if (index < (size >> 1)) {
            // начинаем сначала и идем до него
            Node<E> x = first;
            for (int i = 0; i < index; i++)
                x = x.next;
            return x;
        } else {
            // если в правой части списка
            // то начинаем с конца в обратном направлении
            Node<E> x = last;
            for (int i = size - 1; i > index; i--)
                x = x.prev;
            return x;
        }
    }

    public boolean add(E e) {
        linkLast(e);
        return true;
    }

    void linkLast(E e) {
        // запоминаем последний узел
        final Node<E> l = last;
        // создаем новый узел
        final Node<E> newNode = new Node<>(l, e, null);
        // теперь последний узел списка - это новый узел
        last = newNode;
        // если до этого в списке ничего не было
        if (l == null)
            // то новый узел является первым
            first = newNode;
        else
            // в противном случае новый узел идет сразу за последним (который был до этого)
            l.next = newNode;
        size++;
        modCount++;
    }
}
```

* `Map` - интерфейс, описывающий ассоциативный массив.

```java
public interface Map<K,V> {
    int size();
    
    boolean isEmpty();
    
    boolean containsKey(Object key);
    
    boolean containsValue(Object value);
    
    V get(Object key);
    
    V put(K key, V value);
    
    V remove(Object key);

    void clear();
    // возвращается множество ключей, потому что множество гарантирует уникальность элементов, а ключи - уникальные
    Set<K> keySet();
    // возвращается коллекция, потому что уникальности нет
    Collection<V> values();
    // метод, который возвращает множество пар ключ-значение, поскольку в этих парах ключи уникальные, поэтому мы и возвращаем множество
    Set<Map.Entry<K, V>> entrySet();

    // вложенный интерфейс, который описывают пару ключ-значение
    interface Entry<K,V> {
        K getKey();
        V getValue();
        V setValue(V value);
    }
}
```

* `HashMap` - реализация `Map` на основе хешей.

```java
public class HashMap<K,V> implements Map<K,V> {
    // узел списка в который помещаются элементы
    static class Node<K,V> implements Map.Entry<K,V> {
        final int hash;
        final K key;
        V value;
        Node<K,V> next; 
    }
    // массив связных списков
    Node<K,V>[] table;

    public V put(K key, V value) {
        return putVal(hash(key), key, value, false, true);
    }
    // есть объекты, у которых не очень хорошо реализован хеш-код, например для Double очень часто возникает ситуация
    // кода хеш отличается в старших битах, а в младших он одинаковый
    // поэтому будет порождено много коллизий, потому что нас при расчете индекса интересуют младшие биты
    // для этого функция хеш исправляет такую ситуацию
    static final int hash(Object key) {
        if (key == null) {
            return 0;
        } else {
            int hash = key.hashCode();
            int shifted = hash >>> 16; 
            return hash ^ shifted;
        }
    }

   
}
```

* Хеш-код для "плохих" ключей:

```
int hash = key.hashCode(); // 1111000001110100110111010011010
int shifted = hash >>> 16; // 0000000000000001111000001110100, все 16 первых бит сдвинули на 16 вправо - остальное - нули
return hash ^ shifted; // xor, сложение по модулю, 1 + 0 = 1, 0 + 1 = 1, 0 + 0 = 0, 1 + 1 = 0

1111000001110100110111010011010
0000000000000001111000001110100
1111000001110101001111011101010 -> получили новый исправленный хеш-код, т.е.
 старшие биты сдвинуты на младшие, но при этом за счет операции xor мы не потеряли старшие биты
Теперь у них будет отличаться концовка, следовательно коллизий будет меньше.
``` 

* `Set` - интерфейс, описывающий коллекции, для которых обязательная уникальность элементов.

```java
public interface Set<E> extends Collection<E> {
    // гарантирует, что будет только один уникальный элемент, повторений нет
    boolean add(E e);
}
```

* `HashSet` - класс, реализация `Set` на основе `HashMap`.

```java
public class HashSet<E> implements Set<E> {
    // мапа для хранения значений множества в качестве ключей ассоциативного массива
    private transient HashMap<E,Object> map;
    // просто пустой объект
    private static final Object PRESENT = new Object();
    // когда мы кладем значение во множество, оно кладет это значение в качестве ключа в ассоциативный массив
    public boolean add(E e) {
        return map.put(e, PRESENT)==null;
    }
}
```