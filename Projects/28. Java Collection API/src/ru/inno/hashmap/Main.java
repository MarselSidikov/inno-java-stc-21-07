package ru.inno.hashmap;

import java.util.HashMap;
import java.util.Map;

/**
 * 05.07.2021
 * 28. Java Collection API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Map<Double, String> map = new HashMap<>();
        map.put(0.01, "Комета-1");
        map.put(0.02, "Комета-2");
        map.put(0.03, "Комета-3");
        map.put(0.04, "Комета-4");
        map.put(0.05, "Комета-5");
        map.put(0.06, "Комета-6");
        map.put(0.07, "Комета-7");
        map.put(0.08, "Комета-8");

        for (Double key : map.keySet()) {
            System.out.printf("%4.2f %12d %32s\n", key, key.hashCode(), Integer.toBinaryString(key.hashCode()));
        }
    }
}
