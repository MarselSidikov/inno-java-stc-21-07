package ru.inno.base;

import java.util.Scanner;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        Hen hen = new Hen("Hen-Thread");
        hen.start();
        Egg egg = new Egg("Egg-Thread");
        egg.start();
        Tirex tirex = new Tirex();
        Thread tirexThread = new Thread(tirex);
        tirexThread.start();

        new Thread(() -> {
           for (int i = 0; i < 1000_000; i++) {
               System.out.println(Thread.currentThread().getName() + " Monkey");
           }
        }).start();

        try {
            hen.join();
            egg.join();
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }

        for (int i = 0; i < 1_000_000; i++) {
            System.out.println(Thread.currentThread().getName() + " Human");
        }
    }
}
