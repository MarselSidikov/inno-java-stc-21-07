package ru.inno.base;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Hen extends Thread {

    public Hen(String name) {
        super(name);
    }
    @Override
    public void run() {
        for (int i = 0; i < 1_000_000; i ++) {
            System.out.println(Thread.currentThread().getName() + " Hen");
        }
    }
}
