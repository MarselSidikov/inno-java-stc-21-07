package ru.inno.downloading;

import ru.inno.service.ThreadService;

import java.io.*;
import java.net.URL;
import java.util.Scanner;
import java.util.UUID;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static void saveFile(String link) throws Exception {
        // создаем объект URL для обращения к ссылке в интернете
        URL url = new URL(link);
        // получаем поток байтов из файла по этому урлу
        InputStream in = new BufferedInputStream(url.openStream());
        // создаем вывод, который будет накапливать байты файла, размещенного по URL
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // буфер для изображения
        byte[] buf = new byte[1024];
        int n = 0;
        // считывает блоки байтов в буфер
        while (-1 != (n = in.read(buf))) {
            // потом записывает этот буфер в out
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        // байтовое представление изображения
        byte[] response = out.toByteArray();
        // задаем файлу случайное имя
        String newFileName = UUID.randomUUID().toString();
        // записываем все эти байты в новый файл
        FileOutputStream outputStream = new FileOutputStream("images\\" + newFileName + ".png");
        outputStream.write(response);
        outputStream.close();
        System.out.println("FILE SAVED");
    }

    public static void main(String[] args) throws Exception {
        ThreadService threadService = new ThreadService();

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();

        File file = new File("links.txt");

        BufferedReader reader = new BufferedReader(new FileReader(file));

        String fileUrl = reader.readLine();

        while (fileUrl != null) {
            final String finalFileUrl = fileUrl;
            threadService.submit(() -> {
                try {
                    saveFile(finalFileUrl);
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
            });
            fileUrl = reader.readLine();
        }
    }
}
