package ru.inno.executors;

import java.util.Scanner;

/**
 * 09.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    private static boolean isPrime(int number) {
        if (number == 2 || number == 3) {
            return true;
        }

        for (int j = 2; j * j <= number; j++) {
            if (number % j == 0) {
                return false;
            }
        }

        return true;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TaskExecutor executor = new TaskExecutorThreadPoolImpl(4);
        scanner.nextLine();


        executor.submit(() -> {
            for (int i = 2; i < 500_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " - простое в диапазоне [1]");
                }
            }
        });

        executor.submit(() -> {
            for (int i = 500_000; i < 1_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " - простое в диапазоне [2]");
                }
            }
        });

        executor.submit(() -> {
            for (int i = 1_000_000; i < 2_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " - простое в диапазоне [3]");
                }
            }
        });

        executor.submit(() -> {
            for (int i = 2_000_000; i < 3_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " - простое в диапазоне [4]");
                }
            }
        });

        executor.submit(() -> {
            for (int i = 3_000_000; i < 4_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " - простое в диапазоне [5]");
                }
            }
        });

        executor.submit(() -> {
            for (int i = 4_000_000; i < 5_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " - простое в диапазоне [6]");
                }
            }
        });

        scanner.nextLine();

        executor.submit(() -> {
            for (int i = 6_000_000; i < 7_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " - простое в диапазоне [6]");
                }
            }
        });

        executor.submit(() -> {
            for (int i = 7_000_000; i < 8_000_000; i++) {
                if (isPrime(i)) {
                    System.out.println(Thread.currentThread().getName() + " " + i + " - простое в диапазоне [6]");
                }
            }
        });
    }
}
