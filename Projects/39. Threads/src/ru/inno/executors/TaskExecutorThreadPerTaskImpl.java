package ru.inno.executors;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TaskExecutorThreadPerTaskImpl implements TaskExecutor {
    // для каждой задачи создаем отдельный поток
    @Override
    public void submit(Runnable task) {
        // минус такого подхода
        // 1. Вы можете накидать большое количество submit-ов и получить большое количество потоков
        // от которых не будет смысла, потому что они все будут сильно тормозить систему
        // 2. Создание и завершение потоков - трудоемкая операция для JVM/OS/HARDWARE следовательно
        // порождение N-потоков, а затем удаление этих N-потоков сильно затормозит систему
        Thread thread = new Thread(task);
        thread.start();
    }
}
