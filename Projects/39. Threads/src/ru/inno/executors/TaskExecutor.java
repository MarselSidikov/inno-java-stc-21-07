package ru.inno.executors;

/**
 * 09.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Объекты данного интерфейса могут выполнять задачи в побочных потоках
public interface TaskExecutor {
    /**
     * Запускает задачу в побочном потоке
     * @param task объект, в котором содержится реализация метода run, т.е. именно эта реализация
     *             должна быть выполнена в побочном потоке
     */
    void submit(Runnable task);
}
