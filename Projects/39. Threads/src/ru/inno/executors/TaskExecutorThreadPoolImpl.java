package ru.inno.executors;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 09.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Реализация паттерна ThreadPool - множество потоков, которые выполняют задачи параллельно
// они запускаются сразу при старте программы и работают до конца, принимая новые задачи
public class TaskExecutorThreadPoolImpl implements TaskExecutor {
    // потоки, который выполняет задачи (берет их из очереди)
    private WorkerThread[] threads;
    // очередь задач, берем задачи из этой коллекции по одной и выполняем
    private Deque<Runnable> tasks;

    // вложенный класс, который описывает поток, все время берущий задачи из очереди
    private class WorkerThread extends Thread {
        public WorkerThread(int number) {
            super(String.valueOf("PoolWorker - " + number));
        }

        @Override
        public void run() {
            while (true) {
                // задача потока - взять очередную задачу из очереди
                // раз мы берем задачу из очереди - нам надо ее заблокировать, чтобы никто ее не менял
                Runnable currentTask;
                synchronized (tasks) {
                    // пока очередь пустая, задач нет
                    // WorkerThread должен ждать
                    while (tasks.isEmpty()) {
                        try {
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                    // вытащили задачу из очереди
                    currentTask = tasks.poll();
                }
                // выполняем ее
                currentTask.run();
            }
        }
    }

    public TaskExecutorThreadPoolImpl(int count) {
        // создали очередь на основе связного списка
        this.tasks = new LinkedList<>();
        // создали count-потоков
        threads = new WorkerThread[count];
        // создаем каждый поток и запускаем
        for (int i = 0; i < count; i++) {
            threads[i] = new WorkerThread(i);
            threads[i].start();
        }
    }

    @Override
    public void submit(Runnable task) {
        // как положить задачу в очередь?
        // если мы хотим класть что-то в очередь (изменять)
        // значит надо запретить всем что-то с ней делать в этот момент времени, чтобы не поломать
        synchronized (tasks) {
            // кладем задачу в очередь
            tasks.add(task);
            // оповещаем поток, который ждет на этой очереди
            tasks.notify();
        }
    }
}
