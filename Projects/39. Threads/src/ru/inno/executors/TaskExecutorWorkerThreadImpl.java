package ru.inno.executors;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 09.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Реализация паттерна WorkerThread
// у нас будет один поток, который никогда не будет завершаться
// он будет выполнять задачи в фоне, помимо основного потока main
// он будет брать задачи из очереди и выполнять их последовательно
// поэтому не будет проблем с созданием и завершением большого количества потоков
// но при этом мы используем плюсы дополнительного потока, разгружая main
public class TaskExecutorWorkerThreadImpl implements TaskExecutor {
    // поток, который выполняет задачи (берет их из очереди)
    private WorkerThread workerThread;
    // очередь задач, берем задачи из этой коллекции по одной и выполняем
    private Deque<Runnable> tasks;
    // вложенный класс, который описывает поток, все время берущий задачи из очереди
    private class WorkerThread extends Thread {
        public WorkerThread() {
            super("WorkerThread");
        }

        @Override
        public void run() {
            while(true) {
                // задача потока - взять очередную задачу из очереди
                // раз мы берем задачу из очереди - нам надо ее заблокировать, чтобы никто ее не менял
                synchronized (tasks) {
                    // пока очередь пустая, задач нет
                    // WorkerThread должен ждать
                    while (tasks.isEmpty()) {
                        try {
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                    // вытащили задачу из очереди
                    Runnable currentTask = tasks.poll();
                    // выполняем ее
                    currentTask.run();
                }
            }
        }
    }

    public TaskExecutorWorkerThreadImpl() {
        // создали очередь на основе связного списка
        this.tasks = new LinkedList<>();
        // запустили побочный вечный поток
        this.workerThread = new WorkerThread();
        this.workerThread.start();
    }

    @Override
    public void submit(Runnable task) {
        // как положить задачу в очередь?
        // если мы хотим класть что-то в очередь (изменять)
        // значит надо запретить всем что-то с ней делать в этот момент времени, чтобы не поломать
        synchronized (tasks) {
            // кладем задачу в очередь
            tasks.add(task);
            // оповещаем поток, который ждет на этой очереди
            tasks.notify();
        }
    }
}
