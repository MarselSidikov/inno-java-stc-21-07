package ru.inno.synchronization;

import java.util.Scanner;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        CreditCard card = new CreditCard(10000);
        Human husband = new Human("Муж", card);
        Human wife = new Human("Жена", card);

        husband.start();
        wife.start();
    }
}
