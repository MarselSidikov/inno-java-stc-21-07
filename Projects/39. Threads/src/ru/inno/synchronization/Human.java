package ru.inno.synchronization;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human extends Thread {
    private String name;
    private CreditCard card;

    public Human(String name, CreditCard card) {
        super(name);
        this.name = name;
        this.card = card;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            synchronized (card) {
                if (card.getAmount() > 0) {
                    System.out.println(name + " собирается купить...");
                    if (card.buy(10)) {
                        System.out.println(name + " купил!");
                    } else {
                        System.out.println(name + " говорит ээээээ....");
                    }
                }
            }
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
