package ru.inno.service;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ThreadService {
    public void submit(Runnable task) {
        Thread thread = new Thread(task);
        thread.start();
    }
}
