package ru.inno.service;

import java.util.Scanner;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        ThreadService service = new ThreadService();

        service.submit(() -> {

        });

        Thread.sleep(1000);

        service.submit(() -> {
            for (int i = 0; i < 1_000_000; i++) {
                System.out.println("A");
            }
        });

        Thread.sleep(1000);

        service.submit(() -> {
            for (int i = 0; i < 1_000_000; i++) {
                System.out.println("B");
            }
        });

        Thread.sleep(1000);

        service.submit(() -> {
            for (int i = 0; i < 1_000_000; i++) {
                System.out.println("C");
            }
        });
    }
}
