package ru.inno.classes.service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 12.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        executorService.submit(() -> {
            for (int i = 0; i < 100; i++) {
                System.out.println("Hello");
            }
        });
    }
}
