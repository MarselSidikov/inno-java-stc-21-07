package ru.inno.classes.cyclicbarrier;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

/**
 * 12.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static double saveFIle(String link) {
        try {
            return saveFile0(link);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static double saveFile0(String link) throws Exception {
        // создаем объект URL для обращения к ссылке в интернете
        URL url = new URL(link);
        // получаем поток байтов из файла по этому урлу
        InputStream in = new BufferedInputStream(url.openStream());
        // создаем вывод, который будет накапливать байты файла, размещенного по URL
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        // буфер для изображения
        byte[] buf = new byte[1024];
        int n = 0;
        // считывает блоки байтов в буфер
        while (-1 != (n = in.read(buf))) {
            // потом записывает этот буфер в out
            out.write(buf, 0, n);
        }
        out.close();
        in.close();
        // байтовое представление изображения
        byte[] response = out.toByteArray();
        // задаем файлу случайное имя
        String newFileName = UUID.randomUUID().toString();
        // записываем все эти байты в новый файл
        FileOutputStream outputStream = new FileOutputStream("images\\" + newFileName + ".png");
        outputStream.write(response);
        outputStream.close();
        // возвращаем количество байтов, которое мы скачали сейчас
        return response.length;
    }

    public static void main(String[] args) throws Exception {
        // сохраняем все размеры файлов
        List<Double> fileSizes = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        // завершения скольких потоков мы ждем?

        int parties = scanner.nextInt();
        CyclicBarrier cyclicBarrier = new CyclicBarrier(parties, () -> {
            // что должно произойти, когда parties-потоков завершат работу
            // когда parties-потоков завершило работу, мы с вами считаем сумму размеров всех файлов на текущий момент
            double sum = fileSizes.stream().mapToDouble(Double::doubleValue).sum() / 1024 / 1024;
            System.out.println("Скачано " + parties + " файлов, общий объем - " + sum);
        });

        File file = new File("links.txt");

        BufferedReader reader = new BufferedReader(new FileReader(file));

        String fileUrl = reader.readLine();

        while (fileUrl != null) {
            final String finalFileUrl = fileUrl;

            new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + " начал скачивание");
                double bytesCount = saveFIle(finalFileUrl);
                System.out.println(Thread.currentThread().getName() + " завершил скачивание");
                // запоминаем размер текущего файла, скачанного данным потоком
                fileSizes.add(bytesCount);
                // после того, как мы скачали в этом потоке файл
                System.out.println(Thread.currentThread().getName() + " ушел в ожидание");
                try {
                    // то уводим его в ожидание с помощью cyclicbarrier
                    cyclicBarrier.await();
                } catch (InterruptedException | BrokenBarrierException e) {
                    throw new IllegalStateException(e);
                }
                System.out.println(Thread.currentThread().getName() + " вышел из ожидания");
            }).start();

            fileUrl = reader.readLine();
        }
    }
}
