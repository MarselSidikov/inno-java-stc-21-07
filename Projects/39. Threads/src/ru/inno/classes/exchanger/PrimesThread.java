package ru.inno.classes.exchanger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Exchanger;

/**
 * 12.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// данный поток будет генерировать списки случайных чисел
public class PrimesThread extends Thread{
    // в диапазоне от from до to
    private int from;
    private int to;
    // количество чисел, которые мы должны отправить дальше
    private int partSize;

    private Exchanger<List<Integer>> producer;

    public PrimesThread(int from, int to, int partSize, Exchanger<List<Integer>> producer) {
        super("PrimesThread");
        this.from = from;
        this.to = to;
        this.partSize = partSize;
        this.producer = producer;
    }

    // сумма цифр числа - простое число
    // 724 -> 7 + 2 + 4 = 13
    // 11 -> 2
    public boolean isPrimeSum(int number) {
        int sum = 0;
        // посчитали сумму цифр числа
        while (number != 0) {
            sum += number % 10;
            number = number / 10;
        }

        if (sum == 2 || sum == 3) {
            return true;
        }
        // ищем делитель среди чисел от 2-х до корня из sum
        for (int i = 2; i * i <= sum; i++) {
            if (sum % i == 0) {
                // если делитель нашли - то сумма цифр - не простое число
                return false;
            }
        }

        return true;
    }

    @Override
    public void run() {
        int currentIndex = 0;
        List<Integer> currentPart = new ArrayList<>();
        // проходим весь диапазон чисел
        for (int i = from; i < to; i++) {
            // если сумма цифр данного числа - простое число
            // кидаем его в текущий список
            if (isPrimeSum(i)) {
                currentPart.add(i);
            }

            // если у меня набралось partSize-чисел
            if (currentIndex == partSize) {
                try {
                    System.out.println("PrimesThread - отдает на печать текущую часть");
                    producer.exchange(currentPart);
                    System.out.println("PrimesThread - работает дальше");
                } catch (Exception e) {
                    throw new IllegalStateException(e);
                }
                // делаем новую пустую часть
                currentPart = new ArrayList<>();
                currentIndex = 0;
            }
            currentIndex++;
        }
    }
}
