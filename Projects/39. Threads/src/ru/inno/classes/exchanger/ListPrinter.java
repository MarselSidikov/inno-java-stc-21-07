package ru.inno.classes.exchanger;

import java.util.List;
import java.util.concurrent.Exchanger;

/**
 * 12.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ListPrinter extends Thread {

    private Exchanger<List<Integer>> consumer;

    public ListPrinter(Exchanger<List<Integer>> consumer) {
        super("ListPrinter");
        this.consumer = consumer;
    }

    @Override
    public void run() {
        while (true) {
            List<Integer> current = null;
            System.out.println("ListPrinter - ждет, пока не выполнится расчет");
            try {
                // получили данные от PrimesThread
                current = consumer.exchange(null);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            System.out.println("ListPrinter - получил данные в количестве " + current.size());
        }
    }
}
