package ru.inno.classes.exchanger;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Exchanger;

/**
 * 12.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Exchanger<List<Integer>> exchanger = new Exchanger<>();
        ListPrinter listPrinter = new ListPrinter(exchanger);
        PrimesThread primesThread = new PrimesThread(0, 2_100_000_000, 100_000_000, exchanger);
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        listPrinter.start();
        primesThread.start();
    }
}
