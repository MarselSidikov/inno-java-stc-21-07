package ru.inno.classes.lock;

import java.util.Scanner;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        Lock lock = new ReentrantLock();
        CreditCard card = new CreditCard(10000);
        Human husband = new Human("Муж", card, lock);
        Human wife = new Human("Жена", card, lock);

        husband.start();
        wife.start();
    }
}
