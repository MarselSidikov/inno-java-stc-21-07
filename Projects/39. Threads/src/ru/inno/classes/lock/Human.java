package ru.inno.classes.lock;

import java.util.concurrent.locks.Lock;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human extends Thread {
    private String name;
    private CreditCard card;

    private Lock lock;

    public Human(String name, CreditCard card, Lock lock) {
        super(name);
        this.name = name;
        this.card = card;
        this.lock = lock;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            lock.lock();
                if (card.getAmount() > 0) {
                    System.out.println(name + " собирается купить...");
                    if (card.buy(10)) {
                        System.out.println(name + " купил!");
                    } else {
                        System.out.println(name + " говорит ээээээ....");
                    }
                }
            lock.unlock();
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
