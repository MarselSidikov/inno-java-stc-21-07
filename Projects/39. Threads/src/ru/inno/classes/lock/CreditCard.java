package ru.inno.classes.lock;

/**
 * 05.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CreditCard {
    private int amount;

    public CreditCard(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public boolean buy(int cost) {
        if (cost <= amount) {
            this.amount -= cost;
            return true;
        } else {
            System.out.println("ДЕНЕГ НЕТ");
            return false;
        }
    }
}
