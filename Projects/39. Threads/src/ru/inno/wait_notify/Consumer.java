package ru.inno.wait_notify;

/**
 * 09.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Consumer extends Thread {
    private final Product product;

    public Consumer(Product product) {
        super("Consumer");
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            // данный блок кода использует product в качестве мьютекса и занимает его
            // до тех пор, пока не закончатся операции внутри фигурных скобок
            // следовательно, никакой другой поток влезть между этими операциями не может
            synchronized (product) {
                // пока не подготовлен продукт
                System.out.println("Consumer: проверяет готовность продукта");
                while (!product.isProduced()) {
                    // поток Consumer будет ожидать, пока не произойдет вызов notify для нашего потока
                    System.out.println("Consumer: Продукт не готов. Ушел в ожидание");
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }
                // если наступило событие X и мы вышли из while, значит, что продукт подготовили, можно использовать
                System.out.println("Consumer: Продукт был использован");
                product.consume();
                // оповещаем Producer-а о том, что нужно выйти из wait()
                System.out.println("Consumer: Оповещаем Producer");
                product.notify();

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
