package ru.inno.wait_notify;

import java.util.Scanner;

/**
 * 09.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        Product product = new Product();
        Consumer consumer = new Consumer(product);
        Producer producer = new Producer(product);

        producer.start();
        consumer.start();
    }
}
