package ru.inno.wait_notify;

/**
 * 09.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Producer extends Thread {
    private final Product product;

    public Producer(Product product) {
        super("Producer");
        this.product = product;
    }

    @Override
    public void run() {
        while (true) {
            // данный блок кода использует product в качестве мьютекса и занимает его
            // до тех пор, пока не закончатся операции внутри фигурных скобок
            // следовательно, никакой другой поток влезть между этими операциями не может
            synchronized (product) {
                // пока не использован продукт
                System.out.println("Producer: проверяет состояние продукта");
                while (!product.isConsumed()) {
                    // поток Producer будет ожидать, пока не произойдет оповещение этого потока
                    System.out.println("Producer: Продукт не использован. Ушел в ожидание");
                    try {
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }
                // если наступило событие X и мы вышли из while, значит, что продукт использовали, можно подготовить
                System.out.println("Producer: Продукт подготовлен");
                product.produce();
                // оповещаем потоки (Consumer), которые находятся в состоянии wait о том, что можно выходить из этого состояния
                System.out.println("Producer: оповещаем Consumer");
                product.notify();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }
}
