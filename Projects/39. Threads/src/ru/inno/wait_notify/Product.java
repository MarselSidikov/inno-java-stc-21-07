package ru.inno.wait_notify;

/**
 * 09.08.2021
 * 39. Threads
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Product {
    private boolean isReady;

    // продукт готов к использованию
    public boolean isProduced() {
        return isReady;
    }

    // продукт использован (значит, он не готов)
    public boolean isConsumed() {
        return !isReady;
    }

    // приводим в готовность
    public void produce() {
        this.isReady = true;
    }

    // используем продукт
    public void consume() {
        this.isReady = false;
    }
}
