package ru.inno.repositories;

import java.util.List;

/**
 * 21.07.2021
 * 32. DAO on IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// CRUD -> create, read, update, delete
public interface CrudRepository<T> {
    void save(T model);

    List<T> findAll();
}
