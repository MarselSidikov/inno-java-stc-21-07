package ru.inno.repositories;

import ru.inno.models.User;
import ru.inno.util.IdGenerator;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * 21.07.2021
 * 32. DAO on IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryFileBasedImpl implements UsersRepository {

    private String fileName;
    private IdGenerator idGenerator;

    private Function<String, User> lineToUserFunction = line -> {
        String[] parts = line.split("\\|");
        String email = parts[0];
        String password = parts[1];
        return new User(email, password);
    };

    public UsersRepositoryFileBasedImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
        try (Writer writer = new FileWriter(fileName, true)) {
            // проверка наличия файла
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<User> findByEmail(String email) {
        // try with resources - автоматически закрывает поток, если он открыт
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = reader.readLine();
            while (line != null) {
                User user = lineToUserFunction.apply(line);
                if (user.getEmail().equals(email)) {
                    return Optional.of(user);
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return Optional.empty();
    }

    @Override
    public void save(User model) {
        try {
            model.setId(idGenerator.nextId());
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true));
            writer.write(model.getId() + "|" + model.getEmail() + "|" + model.getPassword() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line = reader.readLine();
            while (line != null) {
                User user = lineToUserFunction.apply(line);
                users.add(user);
                line = reader.readLine();
            }
            return users;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
