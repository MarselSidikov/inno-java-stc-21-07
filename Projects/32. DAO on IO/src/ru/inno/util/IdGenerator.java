package ru.inno.util;

/**
 * 21.07.2021
 * 32. DAO on IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface IdGenerator {
    Integer nextId();
}
