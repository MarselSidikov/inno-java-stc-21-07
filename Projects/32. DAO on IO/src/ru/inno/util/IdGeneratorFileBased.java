package ru.inno.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * 21.07.2021
 * 32. DAO on IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class IdGeneratorFileBased implements IdGenerator {

    private String fileName;

    public IdGeneratorFileBased(String fileName) {
        this.fileName = fileName;
        try {
            Writer writer = new FileWriter(fileName, true);
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Integer nextId() {
        // TODO: доделать, чтобы id хранился в файле и при перезапуске программы он продолжил давать следующие id = пред_id + 1
        return 1;
    }
}
