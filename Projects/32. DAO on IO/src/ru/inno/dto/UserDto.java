package ru.inno.dto;

import ru.inno.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * 07.07.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UserDto {
    private String email;

    public UserDto(String email) {
        this.email = email;
    }

    public static UserDto from(User user) {
        return new UserDto(user.getEmail());
    }

    public static List<UserDto> from(List<User> users) {
        List<UserDto> result = new ArrayList<>();
        for (User user : users) {
            result.add(from(user));
        }
        return result;
    }

    public String getEmail() {
        return email;
    }
}
