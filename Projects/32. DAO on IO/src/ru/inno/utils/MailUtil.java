package ru.inno.utils;

/**
 * 07.07.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface MailUtil {
    void sendMail(String email, String text);
}
