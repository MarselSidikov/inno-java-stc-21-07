package ru.inno.services;

import ru.inno.dto.UserDto;
import ru.inno.models.User;
import ru.inno.repositories.UsersRepository;
import ru.inno.utils.MailUtil;

import java.util.List;
import java.util.Optional;

import static ru.inno.dto.UserDto.from;

/**
 * 07.07.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {
    // зависимость на объект, способный сохранять информацию
    // при этом нам не нужно знать, как он это делает
    private UsersRepository usersRepository;

    private MailUtil mailUtil;

    public UsersServiceImpl(UsersRepository usersRepository, MailUtil mailUtil) {
        this.usersRepository = usersRepository;
        this.mailUtil = mailUtil;
    }

    @Override
    public void signUp(String email, String password) {
        // создаем модель пользователя
        User user = new User(email, password);
        // где-то сохранили пользователя
        usersRepository.save(user);
        mailUtil.sendMail(email, "Вы были успешно зарегистрированы");
    }

    @Override
    public boolean signIn(String email, String password) {
        // получить пользователя по его email-у и проверить, совпадает ли пароль
        // который нам подали на вход
        Optional<User> userOptional = usersRepository.findByEmail(email);
        // если из хранилища данных что-то вернулось - проверяем пароль
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            if (user.getPassword().equals(password)) {
                return true;
            } else {
                System.err.println("Пользователь не прошел проверку по email/password");
                return false;
            }
        }
        System.err.println("Пользователь не прошел проверку по email/password");
        return false;
    }

    @Override
    public List<UserDto> getUsers() {
        List<User> users = usersRepository.findAll();
        return from(users);
    }
}
