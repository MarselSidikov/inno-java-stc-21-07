package ru.inno.services;

import ru.inno.dto.UserDto;

import java.util.List;

/**
 * 07.07.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface UsersService {
    void signUp(String email, String password);

    boolean signIn(String email, String password);

    List<UserDto> getUsers();
}
