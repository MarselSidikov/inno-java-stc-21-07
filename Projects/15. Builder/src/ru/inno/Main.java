package ru.inno;

public class Main {

    public static void main(String[] args) {
//	    NumbersBuilder numbersBuilder = new NumbersBuilder();
//	    NumbersBuilder numbersBuilder1 = numbersBuilder.add(10);
//	    NumbersBuilder numbersBuilder2 = numbersBuilder1.add(10);
//	    NumbersBuilder numbersBuilder3 = numbersBuilder2.add(10);
//	    NumbersBuilder numbersBuilder4 = numbersBuilder3.add(10);
//
//        System.out.println(numbersBuilder4.value());

		NumbersBuilder numbersBuilder = new NumbersBuilder();

		numbersBuilder
				.add(10)
				.add(15)
				.add(20)
				.add(30)
				.add(40);

		System.out.println(numbersBuilder.value());
    }
}
