package ru.inno;

/**
 * 07.06.2021
 * 15. Builder
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NumbersBuilder {
    private int value;

    public NumbersBuilder add(int value) {
        this.value += value;
        return this;
    }

    public int value() {
        return this.value;
    }
}
