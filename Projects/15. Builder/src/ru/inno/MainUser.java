package ru.inno;

/**
 * 07.06.2021
 * 15. Builder
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainUser {
    public static void main(String[] args) {
//        User marsel = new User("Марсель", "Сидиков", 27, true, 1.85, 79);
//        User maria = new User("Мария", null, 0, false, 0.00, 0.0);

        User user = User.builder()
                .firstName("Marsel")
                .lastName("Sidikov")
                .age(26)
                .isWorker(true)
                .build();

        User maria = User.builder()
                .firstName("Мария")
                .age(30)
                .build();

    }
}
