package ru.inno;

/**
 * 01.07.2021
 * 27. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// все ключи должны быть уникальные! как и индексы в массивах
public interface Map<K, V> {
    void put(K key, V value);
    V get(K key);
}
