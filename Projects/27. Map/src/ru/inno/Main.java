package ru.inno;


import ru.inno.primitive.PrimitiveMap;

public class Main {

    public static void main(String[] args) {
		System.out.println(1076425247 & 7);
	    Map<String, Integer> map = new HashMap<>();

	    // array[3] = 6, 3 -> ключ, 6 - значение
	    map.put("Марсель", 27);
	    map.put("Айрат", 22); // коллизия с Марселем
	    map.put("Максим", 22); // OK
	    map.put("Даниил", 21);  // коллизия с Алией
	    map.put("Марсель", 28); // OK
	    map.put("Ильгам", 23); // OK
	    map.put("Салават", 24); // коллизия с Виктором
	    map.put("Алия", 20); // OK
	    map.put("Виктор", 24); // OK

	    // int v = array[3], 3 -ключ, в v сохраним значение
	    int i1 = map.get("Марсель"); // i1 -> 28
	    int i2 = map.get("Айрат"); // i2 -> 22
	    int i3 = map.get("Максим"); // i3 ->  22
	    int i4 = map.get("Даниил"); // i4 -> 21
	    int i5 = map.get("Ильгам"); // i5 -> 23
	    int i6 = map.get("Салават"); // i6 -> 24
	    int i7 = map.get("Алия"); // i7 -> 20
	    int i8 = map.get("Виктор"); // i8 -> 24

		System.out.println(i1);
		System.out.println(i2);
		System.out.println(i3);
		System.out.println(i4);
		System.out.println(i5);
		System.out.println(i6);
		System.out.println(i7);
		System.out.println(i8);
    }
}
