package ru.inno.primitive;

import ru.inno.Map;

/**
 * 01.07.2021
 * 27. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PrimitiveMap<K, V> implements Map<K, V> {
    // почему эта реализация плохая?
    // Потому что добавление и получение ключей/значений выполняется за O(N),
    // где N - это количество элементов

    // как это реализовано в обычных массивах?
    // a[3] = 7
    // адрес(а) + размер(int -> 4) * 3 -> 111 + размер(int - 4) * 3 = 111 + 4 * 3 = 111 + 12 = 123
    // следовательно, мы получаем элемент через обычный массив за O(1)

    // наша задача заключается в том, чтобы научиться "превращать" ключи любого типа в индексы массива,
    // потому что они считаются очень быстро
    private static final int DEFAULT_SIZE = 10;

    private static class MapEntry<K, V> {
        K key;
        V value;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    // массив пар ключ-значение
    private MapEntry<K, V>[] entries;
    private int count;

    public PrimitiveMap() {
        this.entries = new MapEntry[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public void put(K key, V value) {
        MapEntry<K, V> entry = new MapEntry<>(key, value);
        // пробегаем по всем  парам ключ-значение
        for (int i = 0; i < count; i++) {
            // смотрим текущую пару
            MapEntry<K, V> current = entries[i];
            // если у этой пары ключ совпал с тем ключом, который мы подали на вход
            if (current.key.equals(key)) {
                // заменяем в этой паре значение
                current.value = value;
                // останавливаем вызов этого метода
                return;
            }
        }

        this.entries[count] = entry;
        count++;
    }

    @Override
    public V get(K key) {
        // хотим по ключу найти значение

        // пробегаем по всем парам ключ-значение
        for (int i = 0; i < count; i++) {
            // смотрим текущую пару
            MapEntry<K, V> current = entries[i];
            // если у этой пары ключ совпал с тем ключом, который мы подали на вход
            if (current.key.equals(key)) {
                // возвращаем значение внутри этой пары
                return current.value;
            }
        }
        return null;
    }
}
