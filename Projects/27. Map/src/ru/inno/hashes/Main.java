package ru.inno.hashes;

import java.util.Arrays;

/**
 * 01.07.2021
 * 27. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    // для того, чтобы получить числовое представление любого объекта в Java можно использовать
    // метод hashCode, но его нужно переопределить для своего класса

    // важно понимать, что метод hashCode возвращает число int у которого всего
    // 2^32 возможных значений
    // при этом строк, объектов и т.д. явно больше.
    // поэтому могут быть ситуации, когда хеш-коды объектов будут совпадать
    // это называется коллизией, и с этим ничего не поделать

    // hashCode - хеширование, это операция преобразования одной последовательности бит
    // в другую последовательность бит

    // в Java хеширование это преобразование любого объекта в число из диапазона int

    public static int myHashCode(String string) {
        char array[] = string.toCharArray();
        System.out.println(Arrays.toString(array));

        int sum = 0;

        for (int i = 0; i < array.length; i++) {
            sum += array[i];
        }

        return sum;
    }

    // 72
    // 101
    // 108
    // 108
    // 111

    // 1. h1 = 31 * 0 + 72
    // 2. h2 = 31 * (31 * 0 + 72) + 101
    // 3. h3 = 31 * (31 * (31 * 0 + 72) + 101) + 108
    // 4. h4 = 31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108
    // 5. h5 = 31 * (31 * (31 * (31 * (31 * 0 + 72) + 101) + 108) + 108) + 111
    // 31 * 31 * 31 * 31 * 72 + 31 * 31 * 31 * 101 + 31 * 31 * 108 + 31 * 108 + 111
    // 31^4 * 72 + 31^3 * 101 + 31^2 * 108 + 31^1 * 108 + 31^0 * 111
    // hash = sum: code * 31^(size - 1 - i), где i - порядковый номер символа в строке
    // почему степень от порядкового номера? таким образом, мы исключаем коллизии, где буквы поменяли местами
    // почему везде умножаем на 31? Потому что это простое число, и его степени трудно разложить на множители
    // 200 = 100 * 2 = 25 * 4 = 5 * 5 * 4, т.е. больше коллизий
    public static int hashCode(String s) {
        char[] value = s.toCharArray();
        int h = 0;

        for (int i = 0; i < value.length; i++) {
            h = 31 * h + value[i];
        }

        return h;
    }

    public static void main(String[] args) {
        String name = "Марсель";
        String name1 = "Максим";
        String name2 = "лесраМь";
        System.out.println(name.hashCode());
        System.out.println(name1.hashCode());

        System.out.println(myHashCode(name));
        System.out.println(myHashCode(name2));


        char[] hello = "Hello".toCharArray();
        for (int i = 0; i < hello.length; i++) {
            System.out.println((int)hello[i]);
        }

        Human marsel = new Human("Марсель", "Сидиков", 27, 1.85, true);
        Human airat = new Human("Айрат", "Мухутдинов", 22, 1.85, true);
        System.out.println(marsel.hashCode());
        System.out.println(airat.hashCode());
    }


}
