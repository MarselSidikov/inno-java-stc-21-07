package ru.inno.hashes;

/**
 * 02.07.2021
 * 27. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Human {
    private String firstName; // хеш строки - firstName.hashCode();
    private String lastName;
    private int age; // int - хеш-код - это само число
    private double height; // Double.hashCode()
    private boolean isWorker; // Boolean.hashCode()

    public Human(String firstName, String lastName, int age, double height, boolean isWorker) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
        this.isWorker = isWorker;
    }

    @Override
    public int hashCode() {
        int[] hashes = {firstName.hashCode(),
                lastName.hashCode(),
                age,
                Double.hashCode(height),
                Boolean.hashCode(isWorker)};

        int h = 0;

        for (int i = 0; i < hashes.length; i++) {
            h = 31 * h + hashes[i];
        }

        return h;
    }
}
