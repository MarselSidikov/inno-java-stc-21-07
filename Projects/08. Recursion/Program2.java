// https://javarush.ru/groups/posts/1412-formatiruem-vihvod-chisel-v-java
// http://study-java.ru/uroki-java/formatirovanie-chisel-i-texta-v-java/
class Program2 {

	private static int step = 0;

	public static void from1toN(int n) {
		System.out.println("--> from1toN(" + n + ")");
		if (n == 1) {
			System.out.println("<-- from1toN(1)");
			System.out.println(1);
		} else {
			from1toN(n - 1);
			System.out.println(n);
			System.out.println("<-- from1toN(" + n + ")");
		}
	}

	// sum(abcd) = d + sum(abc) = d + c + sum(ab) = d + c + b + sum(a) = d + c + b + a
	public static int digitsSum(int n) {
		if (n < 10) {
			return n;
		} else {
			return n % 10 + digitsSum(n / 10);
		}
	}

	public static int fib(int n) {
		step++;
		System.out.printf("%" + step + "s -->fib(" + n + ")\n", " ");
		if (n == 1 || n == 2) {
			System.out.printf("%" + step + "s <--fib(" + n + ") = 1\n", " ");
			step--;
			return 1;
		}
		int result = fib(n - 1) + fib(n - 2);
		System.out.printf("%" + step + "s <--fib(" + n + ") = " + result + "\n", " ");
		step--;
		return result;
	}

	public static void main(String[] args) {
		// from1toN(10);
		// System.out.println(digitsSum(12345));
		System.out.println(fib(10));
	}
}