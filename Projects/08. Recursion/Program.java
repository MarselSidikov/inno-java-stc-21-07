class Program {

	/**
	n! = 1 * 2 * ... * n
	5! = 1 * 2 * 3 * 4 * 5 = 120

	4! = 1 * 2 * 3 * 4

	5! = (1 * 2 * 3 * 4) * 5

	5! = 4! * 5

	n! = (n - 1)! * n

	0! = 1
	------------------
	**/

	/**
		--> f(5)
			--> f(4)
				--> f(3)
					--> f(2)
						--> f(1)
							--> f(0)
							<-- f(0) = 1
						<-- f(1) = 1
					<-- f(2) = 2
				<-- f(3) = 6
			<-- f(4) = 24
		<-- f(5) = 120
	120
	**/

	public static int f(int n) {
		System.out.println("--> f(" + n + ")");
		if (n == 0) {
			System.out.println("<-- f(" + n + ")" + " = 1");
			// termination
			return 1;
		}
		// (n-1)!
		int previous = f(n - 1);
		// (n-1)! * n
		int result = previous * n;
		System.out.println("<-- f(" + n + ")" + " = " + result);
		return result;
	}

	public static void main(String[] args) {
		System.out.println(f(5));
	}
}