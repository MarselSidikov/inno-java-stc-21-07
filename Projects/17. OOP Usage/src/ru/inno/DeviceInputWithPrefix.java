package ru.inno;

/**
 * 14.06.2021
 * 17. OOP Usage
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceInputWithPrefix implements DeviceInput {

    private DeviceInput deviceInput;

    private String prefix;

    public DeviceInputWithPrefix(String prefix, DeviceInput deviceInput) {
        this.deviceInput = deviceInput;
        this.prefix = prefix;
    }

    @Override
    public String read() {
        return prefix + ": "  + deviceInput.read();
    }

    @Override
    public String getInformation() {
        return "Реализация входного устройства с префиксом";
    }
}
