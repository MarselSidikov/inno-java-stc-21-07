package ru.inno;

/**
 * 14.06.2021
 * 17. OOP Usage
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        DeviceInput scannerDevice = new DeviceInputScannerImpl();
        DeviceInput prefixDevice = new DeviceInputWithPrefix("Сообщение", scannerDevice);
        DeviceInput timeDevice = new DeviceInputWithTime(prefixDevice);
        DeviceOutput deviceOutput = new DeviceOutputErrorImpl();

        IOService service = new IOService(timeDevice, deviceOutput);

        service.printDevicesInformation();

        while (true) {
            String message = service.read();
            service.print(message);
        }
    }
}
