package ru.inno;

/**
 * 14.06.2021
 * 17. OOP Usage
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class IOService {
    private DeviceInput deviceInput;
    private DeviceOutput deviceOutput;

    public IOService(DeviceInput deviceInput, DeviceOutput deviceOutput) {
        this.deviceInput = deviceInput;
        this.deviceOutput = deviceOutput;
    }

    public void print(String message) {
        deviceOutput.write(message);
    }

    public String read() {
        return deviceInput.read();
    }

    public void printDevicesInformation() {
        System.out.println(deviceInput.getInformation());
        System.out.println(deviceOutput.getInformation());
    }
}
