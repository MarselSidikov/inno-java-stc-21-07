package ru.inno;

/**
 * 14.06.2021
 * 17. OOP Usage
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

/**
 * Интерфейс, который описывает устройство ввода/вывода
 */
public interface Device {
    String getInformation();
}
