package ru.inno;

/**
 * 14.06.2021
 * 17. OOP Usage
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface DeviceInput extends Device {
    // позволяет считать откуда-либо сообщение
    String read();
    // String getInformation() - пришла из Device
}
