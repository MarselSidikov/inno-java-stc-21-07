package ru.inno;

/**
 * 14.06.2021
 * 17. OOP Usage
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceInputScannerImpl extends AbstractDeviceInputScannerImpl {
    @Override
    public String getInformation() {
        return "Базовая реализация с использованием Scanner";
    }
}
