package ru.inno;

/**
 * 14.06.2021
 * 17. OOP Usage
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class DeviceOutputErrorImpl implements DeviceOutput {

    @Override
    public void write(String message) {
        System.err.println(message);
    }

    @Override
    public String getInformation() {
        return "Реализация вывода сообщений с помощью System.err-потока";
    }
}
