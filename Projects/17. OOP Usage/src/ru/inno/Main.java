package ru.inno;

import java.util.Scanner;

/**
 * А что, если захотим считывать с файла или интернета?
 * А что, если захотим выводить другим цветом?
 * А что, если захотим добавить текст до или после?
 * и т.д.
 */
public class Main {

    public static void main(String[] args) {
        DeviceInput scannerDevice = new DeviceInputScannerImpl();
        DeviceInput prefixDevice = new DeviceInputWithPrefix("Сообщение", scannerDevice);
        DeviceInput timeDevice = new DeviceInputWithTime(prefixDevice);

        DeviceOutput deviceOutput = new DeviceOutputErrorImpl();

        String message = timeDevice.read();
        deviceOutput.write(message);
    }
}
