package ru.inno;

import java.util.Scanner;

/**
 * 14.06.2021
 * 17. OOP Usage
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class AbstractDeviceInputScannerImpl implements DeviceInput {
    private Scanner scanner;

    public AbstractDeviceInputScannerImpl() {
        this.scanner = new Scanner(System.in);
    }

    public String read() {
        return scanner.nextLine();
    }
}
