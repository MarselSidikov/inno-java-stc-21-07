## Комментарии для проекта lambdas

1. NumbersUtil - класс для обработки чисел, он не знает как обрабатывать числа, но знает как их хранить.

2. Для обработки числа в метод `process` класса NumbersUtil есть возможность передать объект, в котором
реализована логика обработки (объект - потомок `ProcessFunction`)

3. LastDigitProcess - реализация `ProcessFunction` в котором описан метод `process`.

ИТОГО: 

```
// создали объект с реализацией метода process(int number)
ProcessFunction processFunction = new LastDigitProcess();
// в numberUtil передаем число 12345 и объект LastDigitProcess
numbersUtil.process(12345, processFunction);
// numbersUtil в свою очередь вызовет метод process(int number) и получит последнюю цифру числа
// далее он вызовет saveNumber и сохранит его
```
