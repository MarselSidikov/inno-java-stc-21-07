package ru.inno.anons;

/**
 * 15.06.2021
 * 18. Anons And Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NumbersUtilWithEvenProcess extends NumbersUtil {
    @Override
    protected int processNumber(int number) {
        return number % 2;
    }
}
