package ru.inno.lambdas;

/**
 * 15.06.2021
 * 18. Anons And Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();

//        ProcessFunction processFunction = new LastDigitProcess();

        ProcessFunction processFunction = new ProcessFunction() {
            @Override
            public int process(int number) {
                return number % 10;
            }
        };

        // лямбда выражение
        // реализация одного единственного метода функционального интерфейсами
        ProcessFunction sumDigitsFunction = number -> {
            int result = 0;
            while (number != 0) {
                result += number % 10;
                number = number / 10;
            }
            return result;
        };

        ProcessFunction lastDigit = number -> number % 10;

        numbersUtil.process(12345, lastDigit);
        numbersUtil.process(12346, lastDigit);
        numbersUtil.process(12340, lastDigit);
        numbersUtil.process(12349, lastDigit);

        numbersUtil.printProcessed();
    }
}
