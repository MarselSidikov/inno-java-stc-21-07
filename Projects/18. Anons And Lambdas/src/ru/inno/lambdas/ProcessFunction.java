package ru.inno.lambdas;

/**
 * 15.06.2021
 * 18. Anons And Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// интерфейс, в котором определена функция для обработки чисел
// в данном интерфейсе только один метод для реализации
// такой интерфейс называется ФУКНЦИОНАЛЬНЫМ
public interface ProcessFunction {
    int process(int number);
}
