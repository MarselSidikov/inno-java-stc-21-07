package ru.inno.lambdas;

/**
 * 15.06.2021
 * 18. Anons And Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class NumbersUtil {
    // максимальное количество чисел, которое мы можем обработать
    private static final int MAX_PROCESSED_NUMBERS_COUNT = 10;
    // массив для хранения обработанных чисел
    private int processedNumbers[];
    // количество чисел, которое мы уже обработали
    private int processedNumbersCount;

    public NumbersUtil() {
        this.processedNumbers = new int[MAX_PROCESSED_NUMBERS_COUNT];
    }

    public void printProcessed() {
        for (int i = 0; i < processedNumbersCount; i++) {
            System.out.print(processedNumbers[i] + " ");
        }
        System.out.println();
    }

    public void process(int number, ProcessFunction function) {
        if (isHasSize()) {
            // обрабатываю число, не знаю как
            int processedNumber = function.process(number);
            // сохраняю это число
            saveNumber(processedNumber);
        } else {
            System.err.println("Кончилось место для обработки чисел");
        }
    }

    public void process(int first, int second, TwoNumbersFunction function) {
        if (isHasSize()) {
            // обрабатываю числа, не знаю как
            int processedNumber = function.process(first, second);
            // сохраняю это число
            saveNumber(processedNumber);
        } else {
            System.err.println("Кончилось место для обработки чисел");
        }

    }

    private void saveNumber(int number) {
        processedNumbers[processedNumbersCount] = number;
        processedNumbersCount++;
    }

    private boolean isHasSize() {
        return processedNumbersCount < MAX_PROCESSED_NUMBERS_COUNT;
    }
}
