package ru.inno.lambdas;

/**
 * 15.06.2021
 * 18. Anons And Lambdas
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface TwoNumbersFunction {
    int process(int first, int second);
}
