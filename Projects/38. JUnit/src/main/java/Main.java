/**
 * 02.08.2021
 * junit-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();
        System.out.println(numbersUtil.isPrime(21));
        System.out.println(numbersUtil.isPrime(155));
        System.out.println(numbersUtil.isPrime(169));
        System.out.println(numbersUtil.isPrime(27));

        System.out.println(numbersUtil.isPrime(13));
        System.out.println(numbersUtil.isPrime(23));
        System.out.println(numbersUtil.isPrime(31));
        System.out.println(numbersUtil.isPrime(17));
    }
}
