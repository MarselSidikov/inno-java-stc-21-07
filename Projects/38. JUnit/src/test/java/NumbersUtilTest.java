import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 02.08.2021
 * junit-example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {
    // объект который мы тестируем
    private NumbersUtil numbersUtil;

    // данный метод будет запускаться перед каждым тестом
    @BeforeEach
    public void setUp() {
        numbersUtil = new NumbersUtil();
    }

    @Nested
    @DisplayName("isPrime() is working when")
    class ForIsPrime {

        @ParameterizedTest(name = "throws exception on {0}")
        @ValueSource(ints = {0, 1})
        public void on_problems_numbers_throws_exception(int number) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <true> on {0}")
        @ValueSource(ints = {2, 3, 17, 31, 41, 53, 101})
        public void on_prime_numbers_return_true(int number) {
            assertTrue(numbersUtil.isPrime(number));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {169, 121, 289})
        public void on_sqr_numbers_return_false(int number) {
            assertFalse(numbersUtil.isPrime(number));
        }
    }

}