package ru.inno;


import javax.swing.tree.TreeNode;
import java.util.Deque;
import java.util.LinkedList;

/**
 * 24.07.2021
 * 35. Trees
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BinarySearchTreeImpl<T extends Comparable<T>> implements BinarySearchTree<T> {

    private TreeNode<T> root;

    static class TreeNode<E> {
        E value;

        TreeNode<E> left;
        TreeNode<E> right;

        public TreeNode(E value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    @Override
    public void insert(T value) {
        this.root = insert(this.root, value);
    }

    // в дерево, которое заданно корнем root вставляет значение value
    private TreeNode<T> insert(TreeNode<T> root, T value) {
        // если дерево совсем пустое
        if (root == null) {
            root = new TreeNode<>(value);
        } else {
            // если новое значение меньше корня - идем влево
            // если новое значение больше корня - идем вправо
            if (value.compareTo(root.value) < 0) {
                // добавляем элемент в левое поддерево
                root.left = insert(root.left, value);
            } else {
                root.right = insert(root.right, value);
            }
        }
        return root;
    }

    // stack ->
    // [50, 24, 56, 27, 30]
    // stack.push(7)
    // [50, 24, 56, 27, 30, 7]
    // stack.pop() -> 7
    // [50, 24, 56, 27, 30]
    @Override
    public void printDfs() {
        // создаем стек узлов
        Deque<TreeNode<T>> stack = new LinkedList<>();
        // положили корень в стек
        stack.add(root);
        TreeNode<T> current;
        // пока стек не пустой
        while (!stack.isEmpty()) {
            // забираем узел из стека
            current = stack.pop();
            if (current.left != null) {
                // кладем его в очередь
                stack.push(current.left);
            }
            // если у текущего узла есть левый сын
            if (current.right != null) {
                // кладем его в стек
                stack.push(current.right);
            }
            // печатаем узел
            System.out.println(current);
        }
    }

    // queue ->
    // [50, 24, 56, 27, 30]
    // queue.add(7)
    // [7, 50, 24, 56, 27, 30]
    // queue.poll() -> 30
    // [7, 50, 24, 56, 27]

    @Override
    public void printBfs() {
        // создаем очередь узлов
        Deque<TreeNode<T>> queue = new LinkedList<>();
        // положили корень в очередь
        queue.add(root);
        TreeNode<T> current;
        // пока очередь не пустая
        while (!queue.isEmpty()) {
            // забираем узел, который был в очереди
            current = queue.poll();
            // печатаем узел
            System.out.println(current);
            // если у текущего узла есть левый сын
            if (current.left != null) {
                // кладем его в очередь
                queue.add(current.left);
            }
            if (current.right != null) {
                // кладем его в очередь
                queue.add(current.right);
            }
        }
    }
}
