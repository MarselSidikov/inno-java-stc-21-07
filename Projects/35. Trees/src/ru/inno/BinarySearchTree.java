package ru.inno;

/**
 * 24.07.2021
 * 35. Trees
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface BinarySearchTree<T extends Comparable<T>> {
    void insert(T value);
    //обход в глубину
    void printDfs();
    // обход в ширину
    void printBfs();
}
