package ru.inno;

public class LogUtils {
    // массив, сообщение (sort, merge), границы
    public static void log(int array[], String message, int lower, int higher) {

        int i = 0;
        // если у нас числа не входят в сортируемый диапазон - просто выводим их через пробел
        while (i < lower) {
            System.out.print(array[i] + " ");
            i++;
        }
        // печатаем открывающую скобку для левой границы сортировки
        System.out.print("[");
        // печатаем все числа внутри границы
        while (i < higher) {
            System.out.print(array[i] + " ");
            i++;
        }
        // печатаем последнее число
        System.out.print(array[i]);
        i++;
        // печатаем для нее закрывающую скобку
        System.out.print("] ");
        // печатаем оставшиеся числа справа от границы сортировки
        while (i < array.length) {
            System.out.print(array[i] + " ");
            i++;
        }
        // ставим indent штук пробелов
        for (i = 0; i <= indent; i++) {
            System.out.print("  ");
        }
        // выводим само сообщение
        System.out.println(message +"(" + lower + "," + higher + ")");
    }

    // переменная для отступа
    private static int indent;

    static {
        indent = 0;
    }
    // увеличить отступ
    public static void indentUp() {
        indent++;
    }
    // понизить отступ
    public static void indentDown() {
        indent--;
    }
}