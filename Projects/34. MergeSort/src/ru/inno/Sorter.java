package ru.inno;

/**
 * 24.07.2021
 * 34. MergeSort
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Sorter {
    private SortAlgorithm algorithm;

    public Sorter(SortAlgorithm algorithm) {
        this.algorithm = algorithm;
    }

    public void sort(int array[]) {
        algorithm.sort(array);
    }
}
