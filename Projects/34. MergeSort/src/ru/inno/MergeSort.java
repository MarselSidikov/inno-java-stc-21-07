package ru.inno;

/**
 * 24.07.2021
 * 34. MergeSort
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MergeSort implements SortAlgorithm {
    private final static int MAX_ARRAY_SIZE = 100;

    private int helpArray[];

    public MergeSort() {
        this.helpArray = new int[MAX_ARRAY_SIZE];
    }

    @Override
    public void sort(int[] array) {
        sort(array, 0, array.length - 1);
    }

    private void sort(int array[], int lower, int higher) {
        LogUtils.log(array, "sort", lower, higher);
        if (higher <= lower) {
            return;
        }
        // вычисляем середину между lower и higher
        // lower = 15
        // higher = 47
        // middle = 15 + (47 - 15) / 2 = 31
        int middle = lower + (higher - lower) / 2;
        LogUtils.indentUp();
        sort(array, lower, middle); // sort(left)
        sort(array, middle + 1, higher); // sort(right)
        merge(array, lower, middle, higher);
        LogUtils.indentDown();
    }

    private void merge(int array[], int lower, int middle, int higher) {
        // копируем в вспомогательный массив участок от lower до higher
        for (int currentIndex = lower; currentIndex <= higher; currentIndex++) {
            helpArray[currentIndex] = array[currentIndex];
        }

        int i = lower, j = middle + 1;

        for (int currentIndex = lower; currentIndex <= higher; currentIndex++) {
            // если мы уже полностью прошли левую половину
            if (i > middle) {
                // закидываем оставшиеся элементы справа
                array[currentIndex] = helpArray[j];
                j++;
            } else if (j > higher) {
                // если мы уже полностью прошли правую половину
                // закидываем оставшиеся элементы слева
                array[currentIndex] = helpArray[i];
                i++;
            } else if (helpArray[j] < helpArray[i]) {
                // a[j] < a[i], то мы выбираем a[j]
                array[currentIndex] = helpArray[j];
                j++;
            } else {
                // a[j] > a[i], то мы выбираем a[i]
                array[currentIndex] = helpArray[i];
                i++;
            }
        }
        LogUtils.log(array, "merge", lower, higher);
    }
}
