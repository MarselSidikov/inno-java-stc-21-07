package ru.inno;

/**
 * 24.07.2021
 * 34. MergeSort
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SortAlgorithm {
    void sort(int array[]);
}
