package ru.inno;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
	    int array[] = {12, 55, -3, 6, 12, 66, 133, -10};

	    SortAlgorithm algorithm = new MergeSort();
	    Sorter sorter = new Sorter(algorithm);

	    sorter.sort(array);
        System.out.println(Arrays.toString(array));
    }
}
