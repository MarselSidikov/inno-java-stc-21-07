package ru.inno;

/**
 * 17.06.2021
 * 22. Strategy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface SearchAlgorithm {
    boolean search(int element);
}
