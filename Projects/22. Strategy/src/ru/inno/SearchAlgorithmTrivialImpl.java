package ru.inno;

/**
 * 17.06.2021
 * 22. Strategy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SearchAlgorithmTrivialImpl implements SearchAlgorithm {
    private Sequence sequence;

    public SearchAlgorithmTrivialImpl(Sequence sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean search(int element) {
        int[] elements = sequence.toArray();
        for (int i = 0; i < elements.length; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }
}
