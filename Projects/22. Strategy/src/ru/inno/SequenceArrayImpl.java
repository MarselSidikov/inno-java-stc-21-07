package ru.inno;

/**
 * 17.06.2021
 * 22. Strategy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SequenceArrayImpl implements Sequence {
    private static final int THRESHOLD = 5;
    private int[] sequence;

    private SearchAlgorithm searchAlgorithm;

    public SequenceArrayImpl(int[] sequence) {
        this.sequence = sequence;

        if (sequence.length < THRESHOLD) {
            searchAlgorithm = new SearchAlgorithmTrivialImpl(this);
        } else {
            searchAlgorithm = new SearchAlgorithmBinaryImpl(this);
        }
    }

    @Override
    public boolean search(int element) {
        return searchAlgorithm.search(element);
    }

    @Override
    public int[] toArray() {
        return sequence;
    }
}
