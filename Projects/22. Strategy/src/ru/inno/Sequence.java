package ru.inno;

/**
 * 17.06.2021
 * 22. Strategy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Sequence {
    /**
     * Проверяет, если ли элемент в последовательности
     * @param element искомый элемент
     * @return true, если элемент найден
     */
    boolean search(int element);

    /**
     * Получить представление последовательности в виде массива
     * @return массив
     */
    int[] toArray();
}
