package ru.inno.game.repositories;

import ru.inno.game.models.Player;

import java.util.Optional;

/**
 * 07.08.2021
 * 40. GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface PlayersRepository {
    Optional<Player> findByNickname(String nickname);

    // вернет игрока, если он есть, и вернет null, если его нет
    Player getByNickname(String nickname);

    void save(Player player);
    // обновляет данные объекта по ВСЕМ ПОЛЯМ
    // player - {5, a, b, c}, player в БД - {5, x, y, c}
    // update(player) -> player в БД {5, a, b, c}
    void update(Player player);
}
