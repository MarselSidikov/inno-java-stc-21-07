package ru.inno.game.repositories;

import ru.inno.game.models.Game;

import javax.sql.DataSource;

/**
 * 07.08.2021
 * 40. GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class GamesRepositoryJdbcImpl implements GamesRepository {

    private DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {

    }

    @Override
    public Game getById(Long gameId) {
        return null;
    }

    @Override
    public void update(Game game) {

    }
}
