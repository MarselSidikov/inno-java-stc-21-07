package ru.inno.site.repository;

import ru.inno.site.model.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;

/**
 * 29.07.2021
 * 36. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryJdbcImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from account";

    //language=SQL
    private static final String SQL_FIND_BY_ID = "select * from account where id = ?";

    //language=SQL
    private static final String SQL_INSERT = "insert into account(first_name, last_name, age) values (?, ?, ?)";

    private DataSource dataSource;

    public UsersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final Function<ResultSet, User> userMapper = row -> {
        try {
            return User.builder()
                    .id(row.getInt("id"))
                    .firstName(row.getString("first_name"))
                    .lastName(row.getString("last_name"))
                    .age(row.getInt("age"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };


    @Override
    public Optional<User> findFirstByAgeOrderByAgeDesc(Integer age) {
        return Optional.empty();
    }

    @Override
    public void save(User model) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, model.getFirstName());
            statement.setString(2, model.getLastName());
            statement.setInt(3, model.getAge());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new IllegalStateException("Can't insert user");
            }
            // получаю ключи, которые сгенерировала база данных
            ResultSet keys = statement.getGeneratedKeys();

            // если база мне вернула сгенерированный ключ
            if (keys.next()) {
                // запрашиваю сгенерированный ключ, который называется id
                Integer generatedId = keys.getInt("id");
                // проставляю его в модель пользователя
                model.setId(generatedId);
            } else {
                // если база не смогла вернуть сгенерированный ключ
                throw new IllegalStateException("Can't obtain id");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Optional<User> findById(Integer userId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SQL_FIND_BY_ID)) {
            preparedStatement.setInt(1, userId);

            try (ResultSet result = preparedStatement.executeQuery()) {
                // если из базы ничего не пришло
                if (!result.next()) {
                    return Optional.empty();
                }
                // создаем пользователя
                User user = userMapper.apply(result);
                // вернули результат
                return Optional.of(user);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        try (Connection connection = dataSource.getConnection();
             Statement statement = connection.createStatement();
             ResultSet result = statement.executeQuery(SQL_SELECT_ALL)) {
            while (result.next()) {
                User user = userMapper.apply(result);
                users.add(user);
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
        return users;
    }
}
