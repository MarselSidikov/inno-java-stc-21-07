package ru.inno.site;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import ru.inno.site.model.User;
import ru.inno.site.repository.UsersRepository;
import ru.inno.site.repository.UsersRepositoryJdbcImpl;

import javax.sql.DataSource;

/**
 * 29.07.2021
 * 36. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {

        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/java_inno_stc_21_07");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("qwerty007");
        hikariConfig.setMaximumPoolSize(20);

        DataSource dataSource = new HikariDataSource(hikariConfig);

        UsersRepository usersRepository = new UsersRepositoryJdbcImpl(dataSource);

        User user = User.builder()
                .firstName("Владимир")
                .lastName("Мономах")
                .age(56)
                .build();

        usersRepository.save(user);

        System.out.println(user);
        System.out.println(usersRepository.findAll());


    }
}
