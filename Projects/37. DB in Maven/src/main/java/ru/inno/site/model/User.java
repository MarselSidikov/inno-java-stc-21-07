package ru.inno.site.model;

import lombok.*;

import java.util.Objects;
import java.util.StringJoiner;

/**
 * 29.07.2021
 * 36. DB
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode
public class User {
    private Integer id;
    private String firstName;
    private String lastName;
    private Integer age;
}
