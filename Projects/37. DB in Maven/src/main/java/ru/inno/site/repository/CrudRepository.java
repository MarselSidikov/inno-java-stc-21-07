package ru.inno.site.repository;

import java.util.List;
import java.util.Optional;

/**
 * 21.07.2021
 * 32. DAO on IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// CRUD -> create, read, update, delete
public interface CrudRepository<T> {
    void save(T model);

    Optional<T> findById(Integer id);

    List<T> findAll();
}
