package ru.inno.singleton;

import java.util.Scanner;

/**
 * 07.06.2021
 * 13. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
//        Logger logger = new Logger("Logger1");
//        Logger anotherLogger = new Logger("Logger2");

        Logger logger = Logger.getLogger("Logger1");
        Logger anotherLogger = Logger.getLogger("Logger2");

        logger.info("Программа запущена");

        Scanner scanner = new Scanner(System.in);
        logger.info("Создан Scanner для чтения информации");

        int a = scanner.nextInt();
        int b = scanner.nextInt();
        anotherLogger.info("Считано два числа");

        if (b == 0) {
            logger.error("Попытка деления на ноль!");
            anotherLogger.info("Программа остановлена");
            return;
        }
        int result = a / b;
        logger.info("Получен результат работы");
        System.out.println(result);
        anotherLogger.info("Программа завершена");
    }
}
