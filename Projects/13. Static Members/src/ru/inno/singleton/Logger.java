package ru.inno.singleton;

import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * 07.06.2021
 * 13. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// модуль сохраняет информацию о произошедших в системе событиях
public class Logger {

    // переменная, которая хранит в себе единственный экземпляр логгера
    private static Logger instance;

    private String name;

    // приватный конструктор запрещает создание объектов
    private Logger(String name) {
        this.name = name;
    }

    private Logger() {

    }

    // FIXME: НЕ ИСПОЛЬЗОВАТЬ
    // метод для получения единственного экземпляра объекта
    // ленивая инициализация Singleton (по требованию)
    // логгер не будет создан до того момента, пока вы его не попросите
    // у такой реализации есть проблемы
    // что если два компонента одновременно запросят логгер?
//    public static Logger getLogger(String name) {
//        // может быть ситуация, что эта проверка пройдет одновременно
//        if (instance == null) {
//            // одновременно будет создано два объекта
//            instance = new Logger(name);
//        }
//        return instance;
//    }

    // статический инициализатор, который создаст логгер при запуске программы
    // и гарантированно выполнится один раз - это гарантия Java
    static {
        // единственный объект создается в самом начале программы
        instance = new Logger();
    }

    public static Logger getLogger(String name) {
        // если до этого имени не было
        if (instance.name == null) {
            instance.name = name;
        }
        return instance;
    }

    public void info(String message) {
        System.out.println(name + " - " + LocalTime.now() + ": " + message);
    }

    public void error(String message) {
        System.err.println(name + " - " + LocalTime.now() + ": " + message);
    }
}
