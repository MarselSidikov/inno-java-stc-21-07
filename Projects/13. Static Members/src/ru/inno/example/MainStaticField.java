package ru.inno.example;

public class MainStaticField {

    public static void main(String[] args) {
        System.out.println(SomeClass.a);
	    SomeClass object1 = new SomeClass(10);
	    object1.a = 100;
	    SomeClass object2 = new SomeClass(15);
        System.out.println(SomeClass.a);
	    object2.a = 200;
	    SomeClass object3 = new SomeClass(20);
        object3.a = 300;

        System.out.println(object1.b);
        System.out.println(object2.b);
        System.out.println(object3.b);

        // обращаться к этому полю можно (и нужно) напрямую через название класса

        SomeClass.a = 400;

        System.out.println(object1.a);
        System.out.println(object2.a);
        System.out.println(object3.a);
    }
}
