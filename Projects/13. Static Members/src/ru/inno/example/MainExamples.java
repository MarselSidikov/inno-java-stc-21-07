package ru.inno.example;

/**
 * 07.06.2021
 * 13. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainExamples {
    public static void main(String[] args) {
        double sqrt = Math.sqrt(15);
        System.out.println(Character.isDigit('8'));
        double PI = Math.PI;
    }
}
