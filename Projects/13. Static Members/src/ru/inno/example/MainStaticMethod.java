package ru.inno.example;

/**
 * 07.06.2021
 * 13. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainStaticMethod {
    public static void main(String[] args) {
        SomeClass object = new SomeClass(10);
        object.someMethod();
        SomeClass.someStaticMethod();
    }
}
