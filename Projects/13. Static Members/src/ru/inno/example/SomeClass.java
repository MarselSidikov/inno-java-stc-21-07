package ru.inno.example;

import java.util.Random;

/**
 * 07.06.2021
 * 13. Static Members
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class SomeClass {
    public static final int CONST = 10;
    // статическое поле - глобальное для всех объектов
    // поле a - едино для всех объектов
    // начальное значение = 50
    public static int a = 50;
    // данное поле - у каждого объекта свое
    public int b;

    public SomeClass(int b) {
        this.b = b;
        // FIXME: так не делаем!!!
        // поскольку мы разместили в конструкторе
        // присваивание нового значения глобальной переменной
        // оно будет равно 50 после создания любого объекта
        // a = 50;
    }

    // если нам нужна какая-то логика для инициализации глобальной переменной
    // например, загрузка информации из файла, генерация случайного значения и т.д.
    // это - статический инициализатор
    // запускается только один раз при загрузке класса (в начальный момент времени
    // запуска программы)
    // он срабатывает после явного задания значения
    static {
        Random random = new Random();
        a = random.nextInt(1000);
    }

    public void someMethod() {
        System.out.println("Some Method");
        System.out.println(a); // имеет доступ к полю a
        System.out.println(b); // имеет доступ к полю b
    }

    public static void someStaticMethod() {
        System.out.println("Some Static Method");
        System.out.println(a); // имеет доступ к полю а
        // System.out.println(b); // не имеет доступ к полю b
    }

    public static void anotherStaticMethod() {
        someStaticMethod(); // имею доступ к статическому методу
        // someMethod(); // не имею доступ к нестатическому методу
    }
}
