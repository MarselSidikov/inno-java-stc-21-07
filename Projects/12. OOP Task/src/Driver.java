/**
 * 06.06.2021
 * 12. OOP Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Driver {
    private String firstName;
    private String lastName;
    private int experience;

    // ссылка на автобус
    // композиция/агрегирование - объект одного типа включает в себя в качестве поля ссылку на объект другого типа
    private Bus bus;

    public Driver(String firstName, String lastName, int experience) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.experience = experience;
    }

    public void goToBus(Bus bus) {
        this.bus = bus;
        // сказали автобусу, что мы сейчас его водитель
        // this - объектная переменная, которая ссылается на сам объект из которого вызывается метод
        bus.setDriver(this);
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        if (experience >= 0) {
            this.experience = experience;
        } else {
            this.experience = 0;
        }
    }

    public void drive() {
        bus.go();
    }
}
