/**
 * 06.06.2021
 * 12. OOP Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Passenger {
    private String name;

    public Passenger(String name) {
        this.name = name;
    }

    public void goToBus(Bus bus) {
        // просим автобус нас пустить
        bus.addPassenger(this);
    }

    public String getName() {
        return name;
    }
}
