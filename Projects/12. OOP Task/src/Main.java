public class Main {

    public static void main(String[] args) {
	    Driver marsel = new Driver("Марсель", "Сидиков", 5);
	    Driver airat = new Driver("Айрат", "Мухутдинов", 10);

	    Passenger p1 = new Passenger("Максим");
	    Passenger p2 = new Passenger("Даниил");
	    Passenger p3 = new Passenger("Виктор");
	    Passenger p4 = new Passenger("Салават");

	    Bus bus = new Bus(22, "Нефаз");

	    marsel.goToBus(bus);
	    p1.goToBus(bus);
	    p2.goToBus(bus);
	    p3.goToBus(bus);
	    p4.goToBus(bus);

	    marsel.drive();

	    airat.goToBus(bus);
	    p4.goToBus(bus);
    }
}
