/**
 * 06.06.2021
 * 12. OOP Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Bus {
    private static final int MAX_PLACES_COUNT = 3;

    private int number;
    private String model;

    // ссылка на водителя
    private Driver driver;

    private boolean isRun;

    // массив, который хранит всех пассажиров автобуса
    private Passenger[] passengers;
    // текущее количество пассажиров
    private int passengersCount;

    public Bus(int number, String model) {
        if (number >= 1) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        // сделали место для трех пассажиров
        this.passengers = new Passenger[MAX_PLACES_COUNT];
    }

    public void setDriver(Driver driver) {
        if (!isRun) {
            this.driver = driver;
        } else {
            System.err.println("Автобус на линии - невозможно сменить водителя");
        }
    }

    public void addPassenger(Passenger passenger) {
        if (!isRun) {
            // если количество пассажиров меньше максимального
            if (passengersCount < MAX_PLACES_COUNT) {
                // кладем нового пассажира на первое пустое место
                passengers[passengersCount] = passenger;
                passengersCount++;
            } else {
                System.err.println("Мест нет - невозможно пустить пассажира");
            }
        } else {
            System.err.println("Автобус на линии - невозможно пустить пассажира");
        }
    }

    public int getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public void go() {
        this.isRun = true;
        System.out.println("Автобус " + number + " модели " + model + " поехал под управлением " +
                driver.getFirstName());
        for (int i = 0; i < passengersCount; i++) {
            System.out.println("С нами едет: " + passengers[i].getName());
        }
    }

    public void stop() {
        this.isRun = false;
    }
}
