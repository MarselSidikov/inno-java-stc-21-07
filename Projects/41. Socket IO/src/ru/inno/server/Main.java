package ru.inno.server;

/**
 * 16.08.2021
 * 41. Socket IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        GameServer gameServer = new GameServer();
        gameServer.start(7777);

    }
}
