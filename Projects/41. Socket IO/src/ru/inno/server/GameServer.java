package ru.inno.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * 16.08.2021
 * 41. Socket IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class GameServer {
    // поле, которое позволяет реализовать здесь серверное сокет-соединение
    private ServerSocket serverSocket;
    // два игрока нашего сервера
    private ClientThread firstPlayer;
    private ClientThread secondPlayer;

    // запускаем наш серверный сокет на каком-то порту
    public void start(int port) {
        try {
            // создали объект серверного-сокета
            serverSocket = new ServerSocket(port);
            // он уводит текущий поток в ожидание, пока не подключится клиента
            // как только клиент подключится, он будет лежать в объектной переменной client
            System.out.println("SERVER: ожидаем подключения первого игрока");
            firstPlayer = connect();
            System.out.println("SERVER: первый игрок подключен");
            System.out.println("SERVER: ожидаем подключения второго игрока");
            secondPlayer = connect();
            System.out.println("SERVER: второй игрок подключен");
            // запустили побочный поток для работы со вторым клиентом
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private ClientThread connect() {
        try {
            Socket client = serverSocket.accept();
            ClientThread playerThread = new ClientThread(client);
            playerThread.start();
            return playerThread;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private class ClientThread extends Thread {

        private Socket client;
        // поток символов, которые мы отправляем клиенту
        private PrintWriter toClient;
        // поток символов, которые мы читаем от клиента
        private BufferedReader fromClient;

        public ClientThread(Socket client) {
            this.client = client;
            try {
                // обернули потоки байтов в потоки символов
                this.toClient = new PrintWriter(client.getOutputStream(), true);
                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        // мы в любое время можем получить сообщение от клиента
        // поэтому чтение сообщения от клиента должно происходить в побочном потоке
        @Override
        public void run() {
            while (true) {
                String messageFromClient;
                try {
                    // прочитали сообщение от клиента
                    messageFromClient = fromClient.readLine();

                    if (messageFromClient != null) {
                        System.out.println("SERVER: получено сообщение <" + messageFromClient + ">");
                        // Отправить его другому клиенту
                        // если вы - первый клиент
                        if (this == firstPlayer) {
                            // отправляем сообщение второму
                            sendMessage(secondPlayer, messageFromClient);
                        } else {
                            sendMessage(firstPlayer, messageFromClient);
                        }
                    }
                } catch (IOException e) {
                    throw new IllegalStateException();
                }
            }
        }

        public void sendMessage(ClientThread player, String message) {
            player.toClient.println(message);
        }
    }
}
