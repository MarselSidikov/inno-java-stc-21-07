package ru.inno.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

/**
 * 16.08.2021
 * 41. Socket IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // указываем, куда подключаться
        // ip сервера и порт сервера
        try {
            Socket socket = new Socket("localhost", 7777);

            // создадим слушателя сообщений с сервера
            new Thread(() -> {
                try {
                    BufferedReader fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                    String messageFromServer = fromServer.readLine();
                    while (messageFromServer != null) {
                        System.err.println(messageFromServer);
                        messageFromServer = fromServer.readLine();
                    }
                } catch (IOException e) {
                    throw new IllegalStateException();
                }
            }).start();
            // поток символов, который мы будем направлять на сервер
            PrintWriter toServer = new PrintWriter(socket.getOutputStream(), true);

            while (true) {
                String messageToServer = scanner.nextLine();
                toServer.println(messageToServer);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

    }
}
