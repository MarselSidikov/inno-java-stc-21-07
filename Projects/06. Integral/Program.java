class Program {

	public static double f(double x) {
		return x * x * Math.sin(x) * Math.sin(x) * Math.sin(x); 
	}

	public static double calcIntegral(double a, double b, int n) {
		double h = (b - a) / n;

		double integral = 0;

		for (double x = a; x <= b; x += h) {
			double height = f(x);
			double width = h;

			double area = height * width;

			integral += area;
		}

		return integral;
	}

	public static void main(String[] args) {
		double result = calcIntegral(0, 3, 10000);
		System.out.println(result);
	}
}