/**
 * 03.06.2021
 * 10. OOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        User marsel = new User();
        User maxim = marsel;

        marsel.height = 1.90;
        System.out.println(maxim.height);
    }
}
