/**
 * 03.06.2021
 * 10. OOP
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// класс - абстрактный тип данных, шаблон, на основе которого создаются объекты, ссылочный тип
public class User {
    // поля
    // определяют состояние созданных объектов
    double height;
    boolean isWorker;

    // процедуры и функции внутри класса (методы) - определяют поведение
    void work() {
        isWorker = true;
    }

    void relax() {
        isWorker = false;
    }

    void grow(double value) {
        height += value;
    }
}
