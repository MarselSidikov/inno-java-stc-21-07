public class Main {

    public static void main(String[] args) {
        // marsel - это объект?
        // marsel - объектная переменная
        // marsel - указатель (переменная, которая содержит адрес объекта)
	    User marsel = new User(); // User *marsel = new User;
	    marsel.isWorker = false; // marsel->isWorker = false;
	    marsel.height = 1.85;
	    marsel.work();
        System.out.println(marsel.isWorker);

	    User maxim = new User();
        maxim.isWorker = true;
        maxim.height = 1.7;
        maxim.relax();
        System.out.println(maxim.isWorker);

        marsel.grow(0.1);
        System.out.println(marsel.height);
    }
}
