import java.util.Scanner;

class Program {

	public static void printBeauty(int number) {
		System.out.print(" <" + number + "> ");
	} 
	
	public static void printInRange(int from, int to) {
		if (from > to) {
			System.err.println("Incorrect range");
			return; // stop the procedure
		}

		for (int i = from; i <= to; i++) {
			printBeauty(i);
		}
		System.out.println();
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int b = scanner.nextInt();

		printInRange(a, b);

		a = scanner.nextInt();
		b = scanner.nextInt();

		printInRange(a, b);
	}
}