import java.util.Arrays;

class Program3 {
	// в обоих случаях по !!!значению!!!!!
	// 1 - по ссылке или по значению?
	public static void swap(int a, int b) {
		int temp = a;
		a = b;
		b = temp;
	}

	// 2 - по ссылке или по значению?
	public static void swap(int array[], int i, int j) {
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	public static void main(String[] args) {
		// int x = 10;
		// int y = 15;
		// x копируется в a
		// y копируется в b
		// swap(x, y);
		// System.out.println(x + " " + y); // -> 10 15

		int a[] = {1, 2, 3};
		// а (ссылочный тип, адрес) копируется в array
		swap(a, 0, 2);
		System.out.println(Arrays.toString(a)); // -> 3 2 1
	}
}