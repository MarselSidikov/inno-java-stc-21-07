/******************************************************************************

                              Online C++ Compiler.
               Code, Compile, Run and Debug C++ program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <iostream>
#include <string>
using namespace std;

// передача аргумента в формальный параметр по ссылке (адресу)
// передается ссылка на переменную, и получается, что x и a - это одна и та же переменная
void swap(int &a, int &b) {
    cout << &a << " " << &b << endl;
    int temp = a;
    a = b;
    b = temp;
}


// передача аргумента в формальный параметр идет по значению
// значение x копируется в значение a, значение y копируется в значение b
// итого разных 4 переменных
void swapBad(int a, int b) {
    cout << &a << " " << &b << endl;
    int temp = a;
    a = b;
    b = temp;
}


int main()
{
    int x = 10;
    int y = 15;
    swapBad(x,y);
    cout << &x << " " << &y << endl;
    cout << x << " " << y << endl;
}

