class Program2 {

	public static boolean isEven(int number) {
		// if (number % 2 == 0) {
		// 	return true;
		// } else {
		// 	return false;
		// }
		return number % 2 == 0; 
	}

	public static int getSumOfDigits(int number) {
		int sum = 0;
		while (number != 0) {
			sum = sum + number % 10;
			number = number / 10;
		}

		return sum;
	}

	public static void main(String[] args) {
		int result = getSumOfDigits(12345);
		System.out.println(result);

		boolean condition = isEven(123);
	}
}