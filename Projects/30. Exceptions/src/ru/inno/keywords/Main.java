package ru.inno.keywords;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * 09.07.2021
 * 30. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    private static int div(int a, int b) {
        return a / b;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

//        try {
//            int a = scanner.nextInt();
//            scanner.close();
//            int b = scanner.nextInt();
//            // я знаю, что тут может быть исключение
//            System.out.println(div(a, b));
//        } catch (ArithmeticException e) {
//            // этот код среагирует на возникновение исключительной ситуации, в `e` будет помещен объект исключения
//            System.out.println("Какая-то ошибка! Подробнее - " + e.getMessage());
//        } catch (InputMismatchException e) {
//            System.out.println("Видимо что-то с форматом введенных данных!");
//        } catch (IllegalStateException e) {
//            System.out.println("Сканнер уже закрыт!");
//        }

        try {
            int a = scanner.nextInt();
//            scanner.close();
            int b = scanner.nextInt();
            // я знаю, что тут может быть исключение
            System.out.println(div(a, b));
        } catch (ArithmeticException e) {
            // этот код среагирует на возникновение исключительной ситуации, в `e` будет помещен объект исключения
            System.out.println("Какая-то ошибка! Подробнее - " + e.getMessage());
        } catch (RuntimeException e) {
            System.out.println("Что-то пошло не так!");
        }

        System.out.println("Hello!");
    }
}
