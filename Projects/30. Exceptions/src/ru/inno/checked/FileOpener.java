package ru.inno.checked;

import java.io.FileNotFoundException;

/**
 * 09.07.2021
 * 30. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class FileOpener {
    public void openFile(String fileName) throws FileNotFoundException {
        if (fileName.equals("input.txt")) {
            System.out.println("Все ок, я открыл файл");
        } else {
            throw new FileNotFoundException();
        }
    }
}
