package ru.inno.checked;

import java.io.FileNotFoundException;

/**
 * 09.07.2021
 * 30. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        FileOpener opener = new FileOpener();
        try {
            opener.openFile("input.txt");
        } catch (FileNotFoundException e) {
            System.err.println("Ошибка ((");
        }
    }
}
