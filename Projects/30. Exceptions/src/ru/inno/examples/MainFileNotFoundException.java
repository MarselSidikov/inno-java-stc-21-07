package ru.inno.examples;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * 09.07.2021
 * 30. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainFileNotFoundException {
    public static void main(String[] args) {
        InputStream inputStream = null;

        try {
            inputStream = new FileInputStream("input.txt");
        } catch (FileNotFoundException e) {
            // выбрасываем непроверяемое исключение поверх проверяемого, тем самым останавливаем программу
            throw new IllegalArgumentException(e);
        }

        System.out.println("Программа продолжает работать");

        int byteFromFile = -1;

        try {
            byteFromFile = inputStream.read();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        System.out.println(byteFromFile);
    }
}
