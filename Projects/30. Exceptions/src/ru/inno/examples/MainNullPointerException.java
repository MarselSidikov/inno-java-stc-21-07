package ru.inno.examples;

/**
 * 09.07.2021
 * 30. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainNullPointerException {
    public static void main(String[] args) {
        String hello = null;
        System.out.println(hello.toLowerCase());
    }
}
