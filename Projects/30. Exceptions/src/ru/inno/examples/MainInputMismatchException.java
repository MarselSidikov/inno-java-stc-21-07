package ru.inno.examples;

import java.util.Scanner;

/**
 * 09.07.2021
 * 30. Exceptions
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainInputMismatchException {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();
    }
}
