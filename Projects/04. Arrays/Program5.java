import java.util.Scanner;

class Program5 {
	public static void main(String[] args) {
		Scanner scanner = new  Scanner(System.in);
		int numberForSearch = scanner.nextInt();

		boolean isExists = false;

		int numbers[] = {3, 2, 10, -5, 15, 18, -50};

		for (int i = 0; i < numbers.length && !isExists; i++) {
			if (numbers[i] == numberForSearch) {
				isExists = true;
			}
		}

		if (isExists) {
			System.out.println("EXISTS");
		} else {
			System.out.println("NOT EXISTS");
		}
	}

}