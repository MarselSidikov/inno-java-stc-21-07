import java.util.Scanner;

class Program4 {
	public static void main(String[] args) {
		int numbers[] = {3, 2, 10, -5, 15, 18, -50};

		int min = numbers[0];
		int max = numbers[0];

		for (int i = 1; i < numbers.length; i++) {
			if (numbers[i] < min) {
				min = numbers[i];
			}
			if (numbers[i] > max) {
				max = numbers[i];
			}
		}

		System.out.println(min);
		System.out.println(max);
	}
}