import java.util.Scanner;
import java.util.Arrays;

class Program10 {
	public static void main(String[] args) {
		// arrays of arrays
		int a[][] = {
			{77, 89, 20},
			{78, 90, 30},
			{40, 50, 10}
		};

		System.out.println(Arrays.toString(a));
	}
}