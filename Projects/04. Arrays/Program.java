import java.util.Scanner;

class Program {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int age0 = scanner.nextInt();
		int age1 = scanner.nextInt();
		int age2 = scanner.nextInt();
		int age3 = scanner.nextInt();
		int age4 = scanner.nextInt();

		int sum = age0 + age1 + age2 + age3 + age4;

		double average = sum / 5.0; // (10 + 15 + 22 + 43 + 4) / 5 = 18.8
		
		System.out.println(average); // 18.0 -> 18.8
	}
}