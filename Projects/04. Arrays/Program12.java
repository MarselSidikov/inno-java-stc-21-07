import java.util.Random;
import java.util.Arrays;

class Program12 {
	public static void main(String[] args) {
		Random random = new Random();

		int a[][][] = new int[2 + random.nextInt(5)][][];

		for (int i = 0; i < a.length; i++) {
			a[i] = new int[2 + random.nextInt(5)][];
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = new int[2 + random.nextInt(5)];
				for (int k = 0; k < a[i][j].length; k++) {
					a[i][j][k] = random.nextInt(5);
				}
			}
		}

		for (int i = 0; i < a.length; i++) {
			System.out.println("Строка массива  - матрица номер " + i);
			for (int j = 0; j < a[i].length; j++) {
				for (int k = 0; k < a[i][j].length; k++) {
					System.out.print(a[i][j][k] + " ");
				}
				System.out.println();
			}
		}
	}
}