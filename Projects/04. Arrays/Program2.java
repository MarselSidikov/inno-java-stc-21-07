import java.util.Scanner;

class Program2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int maxAgesCount = scanner.nextInt();

		int ages[] = new int[maxAgesCount]; // define array with 5 length

		int i = 0;

		while (i < ages.length) {
			ages[i] = scanner.nextInt();
			i++;
		}

		i = 0;

		int sum = 0;

		while (i < ages.length) {
			sum = sum + ages[i];
			i++;
		}

		double average = (double)sum / ages.length;

		System.out.println(average);
	}
}