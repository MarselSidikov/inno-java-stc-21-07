import java.util.Scanner;
import java.util.Arrays;

class Program11 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int a[][] = new int[3][];
		a[0] = new int[1];
		a[1] = new int[3];
		a[2] = new int[2];

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++) {
				a[i][j] = scanner.nextInt();
			}
		}

		for (int i = 0; i < a.length; i++) {
			System.out.println(Arrays.toString(a[i]));
		}
	}	
}