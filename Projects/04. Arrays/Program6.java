import java.util.Arrays;

class Program6 {
	public static void main(String[] args) {
		int numbers[] = {3, 2, 10, -5, 15, 18, -50};

		System.out.println(Arrays.toString(numbers));
		
		int temp;
		int positionOfMin;
		int min;

		for (int j = 0; j < numbers.length; j++) {
			positionOfMin = j;
			min = numbers[j];

			for (int i = j; i < numbers.length; i++) {
				if (numbers[i] < min) {
					min = numbers[i];
					positionOfMin = i;
				}
			}

			temp = numbers[j];
			numbers[j] = numbers[positionOfMin];
			numbers[positionOfMin] = temp;

			System.out.println("MIN - " + min);
			System.out.println("POSITION OF MIN - " + positionOfMin);
			System.out.println(Arrays.toString(numbers));
		}
	}
}