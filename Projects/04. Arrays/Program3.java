import java.util.Scanner;

class Program3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int maxAgesCount = scanner.nextInt();

		int ages[] = new int[maxAgesCount]; // define array with 5 length

		for (int i = 0; i < ages.length; i++) {
			ages[i] = scanner.nextInt();
		}

		int sum = 0;

		for (int i = 0; i < ages.length; i++) {
			sum = sum + ages[i];
		}

		double average = (double)sum / ages.length;

		System.out.println(average);
	}
}