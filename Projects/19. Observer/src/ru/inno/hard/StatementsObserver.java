package ru.inno.hard;

/**
 * 15.06.2021
 * 19. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// перехватчик заявлений
public class StatementsObserver implements TextObserver {
    private final static String STATEMENT_SIGN = "Заявление";

    @Override
    public void handleDocument(String document) {
        if (document.contains(STATEMENT_SIGN)) {
            System.out.println("Заявление направлено в соответствующий отдел!");
        }
    }
}
