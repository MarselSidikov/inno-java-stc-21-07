package ru.inno.hard;

/**
 * 15.06.2021
 * 19. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CertificateObserver implements TextObserver {

    private final static String CERTIFICATE_SIGN = "Справка";

    @Override
    public void handleDocument(String document) {
        if (document.contains(CERTIFICATE_SIGN)) {
            System.out.println("Справка отправлена на печать!");
        }
    }
}
