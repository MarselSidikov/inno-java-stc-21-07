package ru.inno.hard;

/**
 * 15.06.2021
 * 19. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class TextProcessorImpl implements TextProcessor {
    private static final int MAX_OBSERVERS_COUNT = 5;

    private TextObserver[] observers;
    private int observersCount;

    public TextProcessorImpl() {
        this.observers = new TextObserver[MAX_OBSERVERS_COUNT];
    }

    @Override
    public void addDocument(String document) {
        System.out.println("Получен документ: " + document);
        // оповестить всех обработчиков о том, что пришел документ, а они уже разберутся что с ним делать
        notifyObservers(document);
    }

    @Override
    public void addObserver(TextObserver observer) {
        if (observersCount < MAX_OBSERVERS_COUNT) {
            observers[observersCount] = observer;
            observersCount++;
        } else {
            System.err.println("Превышено допустимое количество обработчиков");
        }
    }

    @Override
    public void notifyObservers(String document) {
        for (int i = 0;i < observersCount; i++) {
            observers[i].handleDocument(document);
        }
    }
}
