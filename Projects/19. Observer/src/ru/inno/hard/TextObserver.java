package ru.inno.hard;

/**
 * 15.06.2021
 * 19. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface TextObserver {
    void handleDocument(String document);
}
