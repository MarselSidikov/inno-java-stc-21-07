package ru.inno.hard;

/**
 * 15.06.2021
 * 19. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface TextProcessor {
    void addDocument(String document);
    // добавляем наблюдателей за документами
    void addObserver(TextObserver observer);
    // оповещает всех наблюдателей о каком-либо событии, связанном с документами
    void notifyObservers(String document);
}
