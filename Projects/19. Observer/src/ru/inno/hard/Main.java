package ru.inno.hard;

import java.time.LocalDateTime;
import java.util.Scanner;

/**
 * 15.06.2021
 * 19. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        TextProcessor textProcessor = new TextProcessorImpl();

        textProcessor.addObserver(new CertificateObserver());
        textProcessor.addObserver(new StatementsObserver());
        textProcessor.addObserver(document -> System.out.println("Документ <" + document + "> был получен в " + LocalDateTime.now().toString()));

        while (true) {
            String document = scanner.nextLine();
            textProcessor.addDocument(document);
        }

    }
}
