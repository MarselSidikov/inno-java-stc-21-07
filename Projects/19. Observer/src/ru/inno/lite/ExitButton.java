package ru.inno.lite;

/**
 * 15.06.2021
 * 19. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ExitButton implements Button {

    private ClickReaction reaction;

    @Override
    public void onClick(ClickReaction reaction) {
        this.reaction = reaction;
    }

    @Override
    public void click() {
        System.out.println("Выходим из приложения");
        // вызываем реакцию наблюдателя
        reaction.handle();
    }
}
