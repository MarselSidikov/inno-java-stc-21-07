package ru.inno.lite;

import ru.inno.lite.ExitButton;

public class Main {

    public static void main(String[] args) {
        ExitButton button = new ExitButton();
        button.onClick(() -> System.exit(255));
        button.click();
    }
}
