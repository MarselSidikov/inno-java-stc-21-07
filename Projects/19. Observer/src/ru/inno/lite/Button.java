package ru.inno.lite;

/**
 * 15.06.2021
 * 19. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Observable - наблюдаемый
public interface Button {
    // задать реакцию на нажатие кнопки
    void onClick(ClickReaction reaction);
    // нажатие кнопки
    void click();
}
