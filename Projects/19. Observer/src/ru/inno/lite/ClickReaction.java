package ru.inno.lite;

/**
 * 15.06.2021
 * 19. Observer
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Observer
public interface ClickReaction {
    void handle();
}
