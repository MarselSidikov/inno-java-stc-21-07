package ru.inno.reader_writer;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.CharBuffer;

/**
 * 19.07.2021
 * 31. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Reader reader = new FileReader("input.txt");
        int fromFile = reader.read();
        char array[] = new char[100];
        CharBuffer buffer = CharBuffer.wrap(array);
        reader.read(buffer);
        System.out.println(new String(buffer.array()));
        System.out.println((char)fromFile);

        Writer writer = new FileWriter("output.txt", true);
        writer.write("Привет, как дела?");

        writer.close();
    }
}
