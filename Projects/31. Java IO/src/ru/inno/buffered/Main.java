package ru.inno.buffered;

import java.io.*;

/**
 * 19.07.2021
 * 31. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Reader reader = new FileReader("input.txt");
        BufferedReader bufferedReader = new BufferedReader(reader);
        System.out.println(bufferedReader.readLine());
        System.out.println(bufferedReader.readLine());
    }
}
