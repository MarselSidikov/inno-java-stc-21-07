package ru.inno.streams;

import java.io.*;
import java.util.Arrays;

/**
 * 19.07.2021
 * 31. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainInputStream {


    public static void main(String[] args) {
        StreamUtil streamUtil = new StreamUtil();

        InputStream input = streamUtil.fileInputStream("input.txt");

        try {
            int byteFromFile = input.read();
            System.out.println(byteFromFile);
            byteFromFile = input.read();
            System.out.println(byteFromFile);
            byteFromFile = input.read();
            System.out.println(byteFromFile);
            byteFromFile = input.read();
            System.out.println(byteFromFile);
            byteFromFile = input.read();
            System.out.println(byteFromFile);
            byteFromFile = input.read();
            System.out.println(byteFromFile);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        try {
            input.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        input = streamUtil.fileInputStream("input.txt");

//        byte[] bytes = streamUtil.readAllBytes(input);

        byte[] bytes;
        int countOfBytes;
        try {
            bytes = new byte[input.available() + 5];
            countOfBytes = input.read(bytes, 3, bytes.length - 3);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        System.out.println("Было прочитано байтов - " + countOfBytes);
        System.out.println(Arrays.toString(bytes));

    }
}
