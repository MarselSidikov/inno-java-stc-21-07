package ru.inno.streams;

import java.io.InputStream;

/**
 * 19.07.2021
 * 31. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainInputStream2 {
    public static void main(String[] args) {
        StreamUtil streamUtil = new StreamUtil();
        InputStream input = streamUtil.fileInputStream("input.txt");
        System.out.println(streamUtil.readAsString(input));
    }
}
