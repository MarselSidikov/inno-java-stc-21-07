package ru.inno.streams;

/**
 * 19.07.2021
 * 31. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // как известно, байт - от 0 до 255
        int byte1 = 255;
        // в java один байт - это -128 до 127
        System.out.println((byte)byte1); // -1
    }
}
