package ru.inno.streams;

import java.io.*;

/**
 * 19.07.2021
 * 31. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class StreamUtil {
    public InputStream fileInputStream(String fileName) {
        try {
            return new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public OutputStream fileOutputSteam(String fileName) {
        try {
            return new FileOutputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public byte[] readAllBytes(InputStream input) {
        byte[] bytes;
        try {
            bytes = new byte[input.available()];
            int currentByte = input.read(); // 0..255 если прочитан байт, -1 если поток кончился

            int i = 0;

            while (currentByte != -1) { // если бы currentByte был объявлен как byte, он мог хранит -1 в двух случаях - либо там 255 либо поток кончился
                bytes[i] = (byte)currentByte;
                currentByte = input.read();
                i++;
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }

        return bytes;
    }

    public String readAsString(InputStream input) {
        try {
            byte[] bytes = new byte[input.available()];
            int size = input.read(bytes);
            return new String(bytes);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
