package ru.inno.streams;

import java.io.IOException;
import java.io.OutputStream;

/**
 * 19.07.2021
 * 31. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MainOutputStream {
    public static void main(String[] args) {
        StreamUtil streamUtil = new StreamUtil();
        OutputStream output = streamUtil.fileOutputSteam("output.txt");
        byte[] bytes = {65, 66, 67, 68, 69};
        try {
            output.write(bytes);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
