package ru.inno.print;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Writer;

/**
 * 19.07.2021
 * 31. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws Exception {
        Writer out = new FileWriter("output.txt");
        PrintWriter writer = new PrintWriter(out);
        writer.printf("%10d + % 10d = %10d\n", 100, 50, 150);
        writer.printf("%10d + % 10d = %10d\n", 50, 60, 110);
        writer.close();
    }
}
