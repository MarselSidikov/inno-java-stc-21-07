# IO Streams

- Классы, которые предназначены для работы с потоками байтов, например - аудио, видео, изображения.
- Для текстовых данных их, как правило, не используют.

## InputStream
* абстрактный класс, описывающий функциональность для работы с потоком байтов.

```java
abstract class InputStream {
    // сам InputStream не описывает то, как будет реализован механизм чтения одного байта.
    public abstract int read() throws IOException;
    
    // метод, который позволяет считать массив байтов в массив b
    public int read(byte b[]) throws IOException {
        // вызывает другой метод, в котором описывается <куда, с какой позиции, сколько байтов>
        return read(b, 0, b.length);
    }
    
    // метод для заполнения массива со смещением
    public int read(byte b[], int off, int len) throws IOException {
        // если массив не задан, неверно заданы параметры, выбрасываем исключения
        if (b == null) {
            throw new NullPointerException();
        } else if (off < 0 || len < 0 || len > b.length - off) {
            throw new IndexOutOfBoundsException();
        } else if (len == 0) {
            return 0;
        }
        // InputStream не знает, как считывается один байт из потока (эта логика описана например в FileInputStream)
        int c = read();
        // считали байт и проверяем, не кончился ли входной поток
        if (c == -1) {
            return -1;
        }
        // записываем первый байт в массив со смещением
        b[off] = (byte)c;

        int i = 1;
        // цикл
        try {
            for (; i < len ; i++) {
                // считали новый байт
                c = read();
                // если поток закончился - то стоп
                if (c == -1) {
                    break;
                }
                // если успешно прочитали, то кладем в массив
                b[off + i] = (byte)c;
            }
        } catch (IOException ee) {
        }
        // возвращаем количество байт, которое прочитали
        return i;
    }


}
```

## FileInputStream

* класс, реализующий функциональность для работы с входным потоком байтов через файлы.

```java
class FileInputStream extends InputStream {

     public int read() throws IOException {
            return read0();
     }
    
     private native int read0() throws IOException;
}
```


## Глобальный вопрос

* Почему при считывании одного байта я считываю `int`, а при считывании массива байтов я считываю `byte`?

* Ответ:

```
Мы знаем, что byte находится в пределах от -128 до 127. Следовательно, байт из потока со значением 255
будет хранится в java как `-1`. НО! Мы также должны сообщить коду, который использует метод read о том,
что поток кончился. Самый логичный для этого вариант, вернуть `-1`. Таким образом, если бы
метод read() возвращал byte, возникала бы коллизия, связанная с тем, что мы бы не могли понять
у нас байт 255 и поэтому `-1` или кончился поток, и поэтому `-1`. Так как int бОльший тип, он 255 сохранит
как 255.
```

# Reader/Writer

- Классы, которые умеют работать с потоком символов.

# Buffered..

```
class Buffered... {
    private InputStream in/Reader in/OutputSteam out/Writer out
    private char cb[]/byte[] buf;
}
```