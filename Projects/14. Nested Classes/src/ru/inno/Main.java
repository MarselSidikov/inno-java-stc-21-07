package ru.inno;

public class Main {

    public static void main(String[] args) {
        Table table = new Table();
        table.put("Марсель", 27);
        table.put("Виктор", 24);
        table.put("Даниил", 21);
        table.put("Айрат", 22);

        // я могу создать объект TableEntry вне класса Table
        // создание экземпляра вложенного класса вне внешнего класса
        Table.TableEntry tableEntry = new Table.TableEntry("Алия", 20);

        // я хочу получить объект, который бы позволил мне просмотреть таблицу поэлементно
        // как это сделать?

        // создаю экземпляр внутреннего класса
        // этот экземпляр привязывается к объекту внешнего класса
        Table.Iterator iterator = table.new Iterator();

        Table.TableEntry element = iterator.next();

        while (element != null) {
            System.out.println(element.key + " " + element.value);
            element = iterator.next();
        }

        Table table1 = new Table();
        table1.put("Audi", 4);
        table1.put("BWM", 3);
        table1.put("Audi TT", 5);

        Table.Iterator anotherIterator = table1.new Iterator();

        element = anotherIterator.next();

        while (element != null) {
            System.out.println(element.key + " " + element.value);
            element = anotherIterator.next();
        }

    }
}
