package ru.inno;

import java.util.Iterator;

/**
 * 07.06.2021
 * 14. Nested Classes
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// внешний класс
public class Table {
    private static final int MAX_TABLE_SIZE = 10;
    // массив всех пар ключ-значение
    private TableEntry[] entries;
    // сколько всего у меня ключей и значений на данный момент
    private int count;

    public Table() {
        this.entries = new TableEntry[MAX_TABLE_SIZE];
        this.count = 0;
    }

    // вложенный (статически вложенный класс)
    // это просто класс, который объявлен внутри другого класса
    public static class TableEntry {
        // одна пара нашей таблицы

        // ключ
        String key;
        // значение
        int value;

        TableEntry(String key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    // внутренний класс (без static)
    // класс, объект которого может пройтись по нашей таблице и получить все записи
    public class Iterator {
        private int current;

        public Iterator() {
            this.current = 0;
        }
        // каждый раз возвращает новый элемент нашей таблицы
        public TableEntry next() {
            if (current == count) {
                return null;
            }
            // я беру новый элемент - запоминаю его
            TableEntry currentEntry = entries[current];
            // перехожу к следующему элементу
            current++;
            // возвращаю тот, что запомнил
            return currentEntry;
        }
    }

    public void put(String key, int value) {
        TableEntry tableEntry = new TableEntry(key, value);
        this.entries[count] = tableEntry;
        count++;
    }
}
