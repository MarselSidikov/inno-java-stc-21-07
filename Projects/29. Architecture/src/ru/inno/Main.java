package ru.inno;

import ru.inno.dto.UserDto;
import ru.inno.repositories.UsersRepository;
import ru.inno.repositories.UsersRepositoryListImpl;
import ru.inno.repositories.UsersRepositoryMapImpl;
import ru.inno.services.UsersService;
import ru.inno.services.UsersServiceImpl;
import ru.inno.utils.MailUtil;
import ru.inno.utils.MailUtilMockImpl;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        UsersRepository usersRepository = new UsersRepositoryMapImpl();
        MailUtil mailUtil = new MailUtilMockImpl();
        UsersService usersService = new UsersServiceImpl(usersRepository, mailUtil);

        boolean isAuthenticated = false;

        while (true) {
            System.out.println("1. Регистрация");
            System.out.println("2. Аутентификация");
            System.out.println("3. Получить список пользователей");

            int command = scanner.nextInt();
            scanner.nextLine();

            switch (command) {
                case 1: {
                    String email = scanner.nextLine();
                    String password = scanner.nextLine();
                    usersService.signUp(email, password);
                    break;
                }
                case 2: {
                    String email = scanner.nextLine();
                    String password = scanner.nextLine();
                    isAuthenticated = usersService.signIn(email, password);
                    break;
                }
                case 3:
                    if (isAuthenticated) {
                        List<UserDto> users = usersService.getUsers();
                        for (UserDto user : users) {
                            System.out.println(user.getEmail());
                        }
                    } else {
                        System.err.println("Вы не прошли аутентификацию");
                    }
                    break;
            }
        }
    }
}
