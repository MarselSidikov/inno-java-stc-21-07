package ru.inno.utils;

/**
 * 07.07.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MailUtilMockImpl implements MailUtil {
    @Override
    public void sendMail(String email, String text) {
        System.err.println("Сообщение <" + text + "> было отправлено на <" + email + ">");
    }
}
