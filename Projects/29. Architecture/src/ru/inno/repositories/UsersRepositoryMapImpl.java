package ru.inno.repositories;

import ru.inno.models.User;

import java.util.*;

/**
 * 07.07.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryMapImpl implements UsersRepository {

    private Map<String, User> users;

    public UsersRepositoryMapImpl() {
        this.users = new HashMap<>();
    }

    @Override
    public void save(User user) {
        this.users.put(user.getEmail(), user);
    }

//    @Override
//    public Optional<User> findByEmail(String email) {
//        User result = users.get(email);
//        if (result != null) {
//            return Optional.of(result);
//        }
//        return Optional.empty();
//    }

    @Override
    public Optional<User> findByEmail(String email) {
        return Optional.ofNullable(users.get(email));
    }

    @Override
    public List<User> findAll() {
        return new ArrayList<>(users.values());
    }
}
