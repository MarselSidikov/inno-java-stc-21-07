package ru.inno.repositories;

import ru.inno.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 07.07.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryListImpl implements UsersRepository {

    private List<User> users;

    public UsersRepositoryListImpl() {
        this.users = new ArrayList<>();
    }

    @Override
    public void save(User user) {
        users.add(user);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        for (User user : users) {
            email = email.toLowerCase();
            String userMail = user.getEmail().toLowerCase();
            if (userMail.equals(email)) {
                // возвращаем объект со значением
                return Optional.of(user);
            }
        }
        // возвращаем пустой объект
        return Optional.empty();
    }

    @Override
    public List<User> findAll() {
        return users;
    }
}
