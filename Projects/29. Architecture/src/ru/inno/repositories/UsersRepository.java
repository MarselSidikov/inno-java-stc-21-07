package ru.inno.repositories;

import ru.inno.models.User;

import java.util.List;
import java.util.Optional;

/**
 * 07.07.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// DAO
public interface UsersRepository {
    void save(User user);
    Optional<User> findByEmail(String email);
    List<User> findAll();
}
