-- чинит последовательность
-- select setval('account_id_seq', 5, true);
drop table if exists driver_car;
drop table if exists car;
drop table if exists account;
-- создание таблицы с пользователями
create table account
(
    -- primary key - первичный ключ, уникальный идентификатор строки, not null + unique, в таблице может быть только один
    id         serial primary key,
    first_name char(20) not null,
    last_name  char(20) not null,
    -- колонка не может быть null, но если в нее все-таки положили значение null, то будет указано значение 1
    age        integer  not null default 1 check (age >= 1 and age <= 120)
);

insert into account(first_name, last_name, age)
values ('Марсель', 'Сидиков', 27);
insert into account (first_name, last_name)
values ('Максим', 'Поздеев');
insert into account(first_name, last_name, age)
values ('Виктор', 'Евлампьев', 24);
insert into account(first_name, last_name, age)
values ('Даниил', 'Вдовинов', 21);
insert into account(first_name, last_name, age)
values ('Айрат', 'Мухутдинов', 22);
insert into account(first_name, last_name, age)
values ('Алия', 'Мухутдинова', 21);
insert into account(first_name, last_name, age)
values ('Аделя', 'Сабирзянова', 19);
insert into account(first_name, last_name, age)
values ('Марсель', 'Сидиков', 20);

-- добавление столбца в таблицу
alter table account
    add email char(30) unique;

-- обновление данных
update account
set email = 'sidikov.marsel@gmail.com'
where id = 1;

-- удаление данных из таблицы
delete
from account
where id = 8;

-- таблица car со связью один ко многим (многие к одному) - много машин у одного владельца,
-- но у машины ровно один владелец
create table car
(
    id       serial primary key,
    color    char(20),
    model    char(20),
    owner_id integer,
    foreign key (owner_id) references account (id)
);

alter table car
    add number char(30) unique;

insert into car(color, model, owner_id)
values ('Black', 'BMW', 1);
insert into car(color, model)
values ('Blue', 'Granta');
insert into car(color, model, owner_id)
values ('Yellow', 'KIA OPTIMA', 1);
insert into car(color, model, number, owner_id)
values ('Красный', 'Lada Largus', 'о111аа16', 2);
insert into car(color, model, number, owner_id)
values ('Серый', 'Renault', 'о222аа16', 3);
insert into car(color, model, number, owner_id)
values ('Голубой', 'Bugatti', 'a001aa01', 4);
insert into car(color, model, number, owner_id)
values ('Синяя', 'LADA', 'o111aa116', 5);
insert into car(color, model, number, owner_id)
values ('Белая', 'Solaris', 'у898aa116', 6);

-- водители и машины, многие ко многим - много водителей могут водить много машин
create table driver_car
(
    driver_id integer,
    car_id    integer,
    foreign key (car_id) references car (id),
    foreign key (driver_id) references account (id)
);

insert into driver_car(driver_id, car_id)
values (2, 2);
insert into driver_car(driver_id, car_id)
values (1, 3);
insert into driver_car(driver_id, car_id)
values (5, 4);
insert into driver_car(driver_id, car_id)
values (4, 6);
insert into driver_car(driver_id, car_id)
values (1, 6);
insert into driver_car(driver_id, car_id)
values (5, 6);
insert into driver_car(driver_id, car_id)
values (4, 5);

-- получаем все колонки всех записей таблицы account
select *
from account;

-- получение только имен пользователей, у которых email не задан
select first_name
from account
where email isnull;

-- получение всех имен и возрастов пользователей, которые старше 23 лет, упор. по убыванию возраста
select first_name, age
from account
where age > 23
order by age desc;

-- получить имена всех владельцев, у которых есть хотя бы одна машина
select first_name
from account a
where a.id in (
    select distinct owner_id -- получаю уникальные id владельцев из таблицы car
    from car
    where owner_id notnull);

-- получение имен владельцев машин, у которых более 1-го водителя

-- 1. получение id машин и количество их водителей
-- 2. получение id машин, у которых более 1-го водителя
-- 3. получение id всех владельцев, у машин которых более 1-го водителя
-- 4. получение имени владельца, у машины которого более 1-го водителя

select first_name as name
from account a
where a.id in (
    select owner_id
    from car c
    where c.id in (
        select car_id
        from (select car_id, count(driver_id) as drivers_count
              from driver_car
              group by car_id) cd
        where drivers_count > 1));

-- left join
select *
from account a
         left join car c on a.id = c.owner_id;

-- right join
select *
from account a
         right join car c on a.id = c.owner_id;

-- inner join
select *
from account a
         inner join car c on a.id = c.owner_id;

-- full join
select *
from account a
         full join car c on a.id = c.owner_id;


