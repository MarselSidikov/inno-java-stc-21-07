package ru.inno.example;

import java.sql.*;
import java.util.Scanner;

public class MainPreparedStatement {
    //language=sql
    private static final String SQL_INSERT_ACCOUNT = "insert into account(first_name, last_name, age) values (?, ?, ?)";

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String firstName = scanner.nextLine();
        String lastName = scanner.nextLine();
        int age = scanner.nextInt();
        try (Connection connection =
                     DriverManager.getConnection("jdbc:postgresql://localhost:5432/java_inno_stc_21_07",
                             "postgres",
                             "qwerty007");
             // позволяет отправлять в БД запросы и получать результат
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_ACCOUNT)) {
            statement.setString(1, firstName);
            statement.setString(2, lastName);
            statement.setInt(3, age);
            int affectedRows = statement.executeUpdate();
            System.out.println("СТРОК ДОБАВЛЕНО: " + affectedRows);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
