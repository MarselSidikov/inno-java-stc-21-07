package ru.inno.example;

import java.io.BufferedReader;
import java.sql.*;

public class MainStatement {

    public static void main(String[] args) {
        try (Connection connection =
                     DriverManager.getConnection("jdbc:postgresql://localhost:5432/java_inno_stc_21_07",
                             "postgres",
                             "qwerty007");
             // позволяет отправлять в БД запросы и получать результат
             Statement statement = connection.createStatement();
             // resultSet - это курсор (паттерн итератор), который позволяет пройти по результирующему множеству запроса
             ResultSet result = statement.executeQuery("select * from account order by id")) {
            while (result.next()) {
                System.out.println(result.getInt("id") + " " + result.getString("first_name"));
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
