package ru.inno.site.repository;

import ru.inno.site.model.User;

import java.util.Optional;

/**
 * 07.07.2021
 * 29. Architecture
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// DAO
public interface UsersRepository extends CrudRepository<User> {
    Optional<User> findFirstByAgeOrderByAgeDesc(Integer age);
}
