import java.util.Scanner;
/**
23
46
72
78
25
13
-1

ODDS COUNT - 3
EVENS COUNT - 3
**/

class Program3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int currentNumber = scanner.nextInt();

		int oddsCount = 0;
		int evensCount = 0;

		while(currentNumber != -1) {
			if (currentNumber % 2 == 0) {
				evensCount++; // evensCount = evensCount + 1, increment
			} else {
				oddsCount++;
			}

			currentNumber = scanner.nextInt();
		}

		System.out.println("ODDS COUNT - " + oddsCount);
		System.out.println("EVENS COUNT - " + evensCount);
	}
}