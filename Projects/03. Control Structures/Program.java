class Program {
	public static void main(String[] args) {
		int number = 56;

		boolean isCondition = number % 2 == 0; 

		if (isCondition) {
			System.out.println("Even");
		} else {
			System.out.println("Odd");
		}
	}
}