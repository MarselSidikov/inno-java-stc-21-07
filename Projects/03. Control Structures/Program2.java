class Program2 {
	public static void main(String[] args) {
		int number = 123235321; // 1 + 2 + 3 + 2 + 3 + 5 ...

		int digitsSum = 0;

		while(number != 0) {
			int lastDigit = number % 10; // 123235321 % 10 -> 1
			digitsSum = digitsSum + lastDigit;
			number = number / 10; // 123235321 / 10 -> 12323532
		}

		System.out.println("Digits sum = " + digitsSum);
	}
}