package ru.inno.files;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 * 23.07.2021
 * 33. Java Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("input.txt"));
            reader.lines()
                    .forEach(System.out::println);
        } catch (FileNotFoundException e) {
            throw new IllegalStateException(e);
        }
    }
}
