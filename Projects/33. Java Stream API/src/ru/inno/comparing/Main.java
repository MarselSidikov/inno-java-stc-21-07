package ru.inno.comparing;

import ru.inno.User;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        User user0 = new User(40L, "Марсель", "Сидиков", 28, 1.85);
        User user1 = new User(7L, "Алия", "Мухутдинова", 21, 1.73);
        User user2 = new User(13L, "Виктор", "Евлампьев", 24, 1.78);
        User user3 = new User(44L, "Айрат", "Мухутдинов", 22, 1.85);
        User user4 = new User(1L, "Даниил", "Вдовинов", 21, 1.75);
        User user5 = new User(15L, "Максим", "Поздеев", 22, 1.68);
        User user6 = new User(9L, "Салават", "Забиров", 25, 1.73);
        User user7 = new User(2L, "Ильгам", "Хасанов", 24, 1.80);

        Scanner scanner = new Scanner(System.in);

        int[] a = {2343, 11, 200, -10, 15, 26};

        Scanner[] scanners = {scanner, scanner, scanner};
        String[] strings = {"Привет", "Арбуз", "Яблоко", "Желтый", "Бит", "Нос"};

        User[] users = {user0, user1, user2, user3, user4, user5, user6, user7};
        BubbleSort.sort(strings);
//        BubbleSort.sort(scanner);

        System.out.println(Arrays.toString(strings));

        Comparator<User> userByIdComparator = (o1, o2) -> Long.compare(o1.getId(), o2.getId());
        Comparator<User> userByHeightComparator = (o1, o2) -> Double.compare(o1.getHeight(), o2.getHeight());

        BubbleSort.sort(users, userByHeightComparator);
        System.out.println(Arrays.toString(users));
    }
}
