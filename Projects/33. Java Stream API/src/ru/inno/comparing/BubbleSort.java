package ru.inno.comparing;

import java.util.Comparator;

/**
 * 23.07.2021
 * 33. Java Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class BubbleSort {
    public static <T extends Comparable<T>> void sort(T[] array) {
        sort(array, null);
    }

    public static <T> void sort(T[] array, Comparator<T> comparator) {
        sort0(array, comparator);
    }

    private static <T> void sort0(T[] array, Comparator<T> comparator) {
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {

                int compareResult;
                // сравниваем
                if (comparator != null) {
                    // если передано правило сравнения - используем его
                    compareResult = comparator.compare(array[j], array[j + 1]);
                } else {
                    // если не передано правило сравнения, очевидно, что они Comparable
                    Comparable<T> a = (Comparable<T>) array[j];
                    T b = array[j + 1];
                    compareResult = a.compareTo(b);
                }

                if (compareResult > 0) {
                    T temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
