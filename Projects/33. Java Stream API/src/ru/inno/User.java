package ru.inno;

import java.util.StringJoiner;

/**
 * 23.07.2021
 * 33. Java Stream API
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class User implements Comparable<User> {
    private Long id;
    private String firstName;
    private String lastName;
    private int age;
    private double height;

    public User(Long id, String firstName, String lastName, int age, double height) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.height = height;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("firstName='" + firstName + "'")
                .add("lastName='" + lastName + "'")
                .add("age=" + age)
                .add("height=" + height)
                .toString();
    }

    // a.compareTo(b)
    // возвращает отрицательное число, если a < b
    // возвращает 0, если a == b
    // возвращает положительное число, если a > b
    // например, a.age = 20, b.age = 30, тогда a.compareTo(b) = -10
    // например, a.age = 20, b.age = 20, тогда a.compareTo(b) = 0
    // например, a.age = 30, b.age = 20, тогда a.compareTo(b) = 10
    @Override
    public int compareTo(User o) {
        return this.age - o.age;
    }
}
