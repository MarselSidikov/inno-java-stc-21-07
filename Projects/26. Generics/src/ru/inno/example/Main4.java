package ru.inno.example;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main4 {
    public static void main(String[] args) {
        Cover<IPhone> iPhoneCover = new Cover<>();
//        iPhoneCover.setPhone(new Nokia3310());
        iPhoneCover.setPhone(new IPhone());

        IPhone iPhoneFromCover = iPhoneCover.getPhone();
        iPhoneFromCover.createPhoto();
//        Nokia3310 nokia3310FromCover = iPhoneCover.getPhone();

        Cover<Nokia3310> nokia3310Cover = new Cover<>();
        nokia3310Cover.setPhone(new Nokia3310());

        Nokia3310 nokia3310FromCover = nokia3310Cover.getPhone();
        nokia3310FromCover.breakWall();
    }
}
