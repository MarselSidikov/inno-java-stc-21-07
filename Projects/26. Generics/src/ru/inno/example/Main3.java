package ru.inno.example;

import java.util.Scanner;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        CoverForObject coverForObject = new CoverForObject();

        Nokia3310 nokia3310 = new Nokia3310();
        IPhone iPhone = new IPhone();

        coverForObject.setPhone(nokia3310);
        coverForObject.setPhone(new Scanner(System.in));

        // я думал, что в чехле лежит IPhone, но там был Nokia, произошла ошибка преобразования
        IPhone iPhoneFromCover = (IPhone)coverForObject.getPhone();
    }
}
