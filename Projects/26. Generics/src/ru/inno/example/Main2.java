package ru.inno.example;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        CoverForObject coverForObject = new CoverForObject();

        Nokia3310 nokia3310 = new Nokia3310();
        IPhone iPhone = new IPhone();

        coverForObject.setPhone(nokia3310);
        Nokia3310 nokia3310FromCover = (Nokia3310)coverForObject.getPhone();
        nokia3310FromCover.breakWall();


        coverForObject.setPhone(iPhone);
        IPhone iPhoneFromCover = (IPhone)coverForObject.getPhone();
        iPhoneFromCover.createPhoto();


    }
}
