package ru.inno.example;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CoverForObject {
    private Object phone;

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public Object getPhone() {
        return this.phone;
    }
}
