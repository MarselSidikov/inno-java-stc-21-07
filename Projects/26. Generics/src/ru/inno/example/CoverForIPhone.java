package ru.inno.example;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CoverForIPhone {
    private IPhone iPhone;

    public void setPhone(IPhone iPhone) {
        this.iPhone = iPhone;
    }

    public IPhone getPhone() {
        return this.iPhone;
    }
}
