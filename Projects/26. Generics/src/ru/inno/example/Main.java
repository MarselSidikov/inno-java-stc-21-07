package ru.inno.example;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        CoverForIPhone coverForIPhone = new CoverForIPhone();
        IPhone iPhone = new IPhone();

        coverForIPhone.setPhone(iPhone);

        IPhone iPhoneFromCover = coverForIPhone.getPhone();
        iPhoneFromCover.createPhoto();

        CoverForNokia3310 coverForNokia3310 = new CoverForNokia3310();
        Nokia3310 nokia3310 = new Nokia3310();

        coverForNokia3310.setPhone(nokia3310);

        Nokia3310 nokia3310FromCover = coverForNokia3310.getPhone();
        nokia3310FromCover.breakWall();
    }
}
