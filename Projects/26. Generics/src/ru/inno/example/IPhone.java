package ru.inno.example;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class IPhone {
    public void createPhoto() {
        System.out.println("Фото сделано!");
    }
}
