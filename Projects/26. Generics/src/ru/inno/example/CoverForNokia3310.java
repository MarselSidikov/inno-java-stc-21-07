package ru.inno.example;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CoverForNokia3310 {
    private Nokia3310 nokia3310;

    public void setPhone(Nokia3310 nokia3310) {
        this.nokia3310 = nokia3310;
    }

    public Nokia3310 getPhone() {
        return this.nokia3310;
    }
}
