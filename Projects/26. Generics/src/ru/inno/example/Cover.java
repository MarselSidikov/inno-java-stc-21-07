package ru.inno.example;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Cover<T> {
    private T phone;

    public void setPhone(T phone) {
        this.phone = phone;
    }

    public T getPhone() {
        return this.phone;
    }
}
