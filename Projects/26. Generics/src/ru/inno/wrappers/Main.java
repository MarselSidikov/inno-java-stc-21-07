package ru.inno.wrappers;

import java.util.ArrayList;
import java.util.List;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // Character, Boolean, Double, Short, Long
        // оберточный тип позволяет использовать примитивные значения с обобщениями
        // boxing
        Integer i1 = new Integer(10);
        // unboxing
        int i2 = i1.intValue();

        // autoboxing
        Integer i3 = 155;
        // autounboxing
        int i4 = i3;
        List<Integer> list = new ArrayList<>();
        list.add(5);
    }
}
