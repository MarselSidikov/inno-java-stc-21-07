package ru.inno.extended;

import java.util.StringJoiner;

/**
 * 01.07.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Animal {
    public void who() {
        System.out.println("Животное");
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Animal.class.getSimpleName() + "[", "]")
                .toString();
    }
}
