package ru.inno.extended;

/**
 * 01.07.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Dog extends Wolf {
    @Override
    public void who() {
        System.out.println("Собака");
    }

    public void sitDown() {
        System.out.println("Сажусь!");
    }
}
