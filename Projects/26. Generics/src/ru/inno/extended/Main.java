package ru.inno.extended;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 01.07.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    // несмотря на то, что Animal - предок тигров, волков и т.д.
    // List<Animal> не является предком List<Wolf>, List<Dog>
    // почему?
    // потому что этот метод позволяет добавлять в список животных объекты всех типов-потомков
    // а что если сюда передадут список собак? Это значит я могу в него положить волка? но это неправильно
    // а почему плохо, если в список собак добавить волка?
    // это плохо потому, что собака может содержать метод, которого нет у волка
    // и если где-то есть метод, который работает со списком собак, он может вызывать этот специфичный метод
    public static void forAnimalList(List<Animal> animals) {
        for (int i = 0; i < animals.size(); i++) {
            Animal animal = animals.get(i);
            animal.who();
        }
        animals.add(new Wolf());
        animals.add(new Dog());
        animals.add(new GoodBoy());
//        animals.add(new Scanner(System.in));
    }

    // если например сначала вызвать forAnimalList с параметром Lit<Dog>
    // то forAnimalList может добавить например волка, что логично, потому что волк потомок животного
    // но тогда список будет поврежден, и среди собак окажется волк
    // а у волка нет метода sitDown и произойдет ошибка
    public static void forDogsList(List<Dog> dogs) {
        for (int i = 0; i < dogs.size(); i++) {
            Dog dog = dogs.get(i);
            dog.sitDown();
        }
    }

    // какие параметры я могу передать в raw-тип?
    // могу передать какой угодно список
    // т.е. List является предком всех других списков
    public static void forRawList(List list) {
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            // я не знаю какого типа на самом деле объект, поэтому такие преобразования могут поломать программу
            // raw-типы не рекомендуется использовать вообще
            ((Animal)object).who();
        }
        // здесь я могу добавить в список любой объект -> поломать список
        // поэтому тоже не рекомендуется использовать такой raw-тип
        list.add(new Wolf());
        list.add(new Scanner(System.in));
    }

    // аналогично List<Animal> сюда нельзя класть ни один список не Object
    public static void forObjectsList(List<Object> objects) {
        for (int i = 0; i < objects.size(); i++) {
            Object object = objects.get(i);
            Animal animal = (Animal) object;
            animal.who();
        }
    }
    // List<?> как и просто List является предком всех списков
    // используем тогда, когда вам не важно, что там за объекты, вы работаете с Object
    // добавлять ничего нельзя, поэтому этот метод ничего не поломает в качестве побочного эффекта
    public static void forWildcardList(List<?> list) {
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            // я не знаю какого типа на самом деле объект, поэтому такие преобразования могут поломать программу
            // raw-типы не рекомендуется использовать вообще
            System.out.println(object.toString());
        }
        // в wildcard-список нельзя ничего добавлять, т.е. список поломать вы не можете
//        list.add(new Scanner(System.in));
//        list.add(new Wolf());
    }

    // UPPERBOUNDS - ограничение сверху
    // могу передавать списки, которые содержать объекты потомков класса Dog или самого Dog
    public static void forUpperBoundsDogsList(List<? extends Dog> dogs) {
        for (int i = 0; i < dogs.size(); i++) {
            Dog dog = dogs.get(i);
            dog.sitDown();
        }
        // не могу класть ничего, потому что могу поломать список, если там были хорошие мальчики,
        // я буду иметь возможность положить собаку, а это поломает список
//        dogs.add(new Dog());
//        dogs.add(new GoodBoy());
    }

    // LOWERBOUNDS
    public static void forLowerBoundsWolvesList(List<? super Wolf> list) {
        for (int i = 0; i < list.size(); i++) {
            // почему мы не можем получить ничего, кроме Object?
            // потому что мы не знаем в принципе что там может быть
//            Wolf wolf = list.get(i);
//            Animal animal = list.get(i);
            Object object = list.get(i);
        }
        // не могу класть объекты, которые не являются потомками Wolf
        // а почему могу класть потомков? Потому что список состоит точно из предков
        // могу положить волка
        list.add(new Wolf());
        // могу положить goodboy
        list.add(new GoodBoy());
        list.add(new Dog());
//        list.add(new Animal());
//        list.add(new Scanner(System.in));
    }

    public static void main(String[] args) {
        Animal animal = new Animal();
        Tiger tiger = new Tiger();
        Wolf wolf = new Wolf();
        Dog dog = new Dog();
        GoodBoy goodBoy = new GoodBoy();

        List<Animal> animals = new ArrayList<>();
        animals.add(animal);
        animals.add(tiger);
        animals.add(wolf);
        animals.add(dog);
        animals.add(goodBoy);

        List<Tiger> tigers = new ArrayList<>();
        tigers.add(tiger);

        List<Wolf> wolves = new ArrayList<>();
        wolves.add(wolf);
        wolves.add(dog);
        wolves.add(goodBoy);

        List<Dog> dogs = new ArrayList<>();
        dogs.add(dog);
        dogs.add(goodBoy);
//        dogs.add(wolves);

        List<GoodBoy> goodBoys = new ArrayList<>();
        goodBoys.add(goodBoy);

        forAnimalList(animals);
//        forAnimalList(tigers);
//        forAnimalList(wolves);
//        forAnimalList(dogs);
//        forAnimalList(goodBoy);

        forRawList(animals);
        forRawList(dogs);
        forRawList(wolves);
        forRawList(tigers);

        List<Scanner> scanners = new ArrayList<>();
        scanners.add(new Scanner(System.in));
//        forRawList(scanners);

//        forObjectsList(animals);
//        forObjectsList(wolves);

//        forDogsList(goodBoy);

        System.out.println("WILDCARD");

        forWildcardList(animals);
        forWildcardList(dogs);
        forWildcardList(wolves);
//        forWildcardList(scanners);

        forUpperBoundsDogsList(dogs);
        forUpperBoundsDogsList(goodBoys);

        List<Object> objects = new ArrayList<>();
        objects.add(new Scanner(System.in));
        forLowerBoundsWolvesList(animals);
        forLowerBoundsWolvesList(wolves);
//        forLowerBoundsWolvesList(dogs);
//        forLowerBoundsWolvesList(tigers);
//        forLowerBoundsWolvesList(scanners);
        forLowerBoundsWolvesList(objects);
    }
}
