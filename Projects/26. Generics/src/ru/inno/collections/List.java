package ru.inno.collections;

/**
 * 21.06.2021
 * 23. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 *
 * Список это такая коллекция, где сохраняется порядок добавления элементов.
 * Если вы добавили элемент 6-м, то он и будет доступен под 6-м номеров
 */
// List<String> -> Collection<String> -> Iterator<String> -> String next();
public interface List<C> extends Collection<C> {

    /**
     * Возвращает элемент под заданным индексом
     * @param index индекс элемента
     * @return элемент
     */
    C get(int index);

    void addFirst(C element);

    // void add(C element);
}
