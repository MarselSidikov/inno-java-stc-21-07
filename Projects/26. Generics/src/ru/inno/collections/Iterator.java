package ru.inno.collections;

/**
 * 21.06.2021
 * 23. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Iterator<B> {
    /**
     * перейти к следующему элементу
     * @return элемент
     */
    B next();

    /**
     * проверить, есть ли следующий элемент
     * @return true если следующий элемент есть, false - если следующего элемента нет
     */
    boolean hasNext();
}
