package ru.inno.collections;

/**
 * 28.06.2021
 * 26. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();
        stringList.add("Марсель");
        stringList.add("Максим");
        stringList.add("Алия");
        stringList.add("Виктор");
        stringList.add("Даниил");
        stringList.add("Айрат");
//        stringList.add(134);

        Iterator<String> iterator = stringList.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        List<Integer> integersList = new ArrayList<>();
        integersList.add(27);
        integersList.add(21);
        integersList.add(20);
        integersList.add(24);
        integersList.add(21);
        integersList.add(22);
//        stringList.add(134);

        Iterator<Integer> integerIterator = integersList.iterator();

        while (integerIterator.hasNext()) {
            System.out.println(integerIterator.next());
        }

    }
}
