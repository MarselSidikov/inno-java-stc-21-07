package ru.inno.collections;


/**
 * 21.06.2021
 * 23. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class ArrayList<D> implements List<D> {
    private static final int DEFAULT_SIZE = 10;
    // ссылка на массив с элементами
    private D[] elements;

    private int size;

    public ArrayList() {
        this.elements = (D[])new Object[DEFAULT_SIZE];
        this.size = 0;
    }

    @Override
    public D get(int index) {
        if (index >= 0 && index < size) {
            return elements[index];
        } else {
            System.err.println("Out of bounds");
            return null;
        }
    }

    @Override
    public void addFirst(D element) {
        if (size == elements.length) {
            resize();
        }
        // делаем сдвиг элементов массива на 1 вправо
        for (int i = size + 1; i > 0; i--) {
            this.elements[i] = this.elements[i - 1];
        }

        this.elements[0] = element;
        size++;
    }

    @Override
    public void add(D element) {
        // если элементов стало столько, сколько в принципе может вместиться в этот массив
        if (size == elements.length) {
            resize();
        }

        this.elements[size] = element;
        size++;
    }

    private void resize() {
        // создали массив, размер которого в полтора раза больше массива, в котором у нас сейчас лежат элементы
        D[] newElements = (D[])new Object[elements.length + elements.length / 2];
        // копируем данные из массива с элементами в новый массив, больший по размеру
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }
        // изменяем ссылку на массив элементами
        this.elements = newElements;
    }

    @Override
    public boolean contains(D element) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void remove(D element) {

    }

    // внутренний класс
    private class ArrayListIterator implements Iterator<D> {
        // текущая позиция итератора
        private int current = 0;

        @Override
        public D next() {
            // запомнили элемент, который хотим вернуть
            D element = elements[current];
            // сдвигаем позицию итератора
            current++;
            // возвращаем элемент
            return element;
        }

        @Override
        public boolean hasNext() {
            // пока не дошли до конца списка, возвращаем true
            return current < size;
        }
    }

    @Override
    public Iterator<D> iterator() {
        return new ArrayListIterator();
    }
}
