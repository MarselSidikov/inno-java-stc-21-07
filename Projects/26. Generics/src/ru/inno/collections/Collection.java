package ru.inno.collections;

/**
 * 21.06.2021
 * 23. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 *
 * Данный интерфейс подразумевает возможность сохранять элементы в каком-либо наборе
 * При этом порядок (кого и за кем добавили) элементов не играет роли
 */
public interface Collection<A> {
    /**
     * Добавляет элемент в коллекцию
     * @param element добавляемый элемент
     */
    void add(A element);

    /**
     * Проверяет, есть ли элемент в коллекции
     * @param element искомый элемент
     * @return true, если элемент присутствует хотя бы один раз в коллекции
     */
    boolean contains(A element);

    /**
     * Получаем количество элементов в коллекции
     * @return возвращает число элементов
     */
    int size();

    /**
     * Удаляет первое вхождение элемента в список
     * @param element удаляемый элемент
     */
    void remove(A element);

    Iterator<A> iterator();
}
