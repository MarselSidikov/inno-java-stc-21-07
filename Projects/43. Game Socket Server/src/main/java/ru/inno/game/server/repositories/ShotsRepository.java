package ru.inno.game.server.repositories;

import ru.inno.game.server.models.Shot;

/**
 * 07.08.2021
 * 40. GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface ShotsRepository {
    void save(Shot shot);
}
