package ru.inno.game.server.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 07.08.2021
 * 40. GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Player {
    private Long id;
    private String ip;
    private String nickname;
    private Integer points;
    private Integer winsCount;
    private Integer losesCount;
}
