package ru.inno.game.server.repositories;

import ru.inno.game.server.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;

/**
 * 23.08.2021
 * 43. Game Socket Server
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class PlayersRepositoryJdbcImpl implements PlayersRepository {

    //language=SQL
    private static String SQL_FIND_BY_NICKNAME = "select id, ip, name, points from player where name = ?";

    //language=SQL
    private static String SQL_INSERT = "insert into player(name, ip) values (?, ?)";

    private DataSource dataSource;

    public PlayersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Optional<Player> findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME)) {
            statement.setString(1, nickname);
            try (ResultSet result = statement.executeQuery()){
                if (result.next()) {
                    return Optional.of(Player.builder()
                            .id(result.getLong("id"))
                            .nickname(result.getString("name"))
                            .ip(result.getString("ip"))
                            .points(result.getInt("points"))
                            .build());
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, player.getNickname());
            statement.setString(2, player.getIp());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert player");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                player.setId(generatedKeys.getLong("id"));
            } else {
                throw new SQLException("Can't obtain id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        // TODO: доделать обновление IP-адреса, если такой игрок уже был (+ обновлять очки = количеству попаданий всего)
        System.out.println("ПРОИСХОДИТ ОБНОВЛЕНИЕ IP " + player.getNickname() + " " + player.getIp());
    }
}
