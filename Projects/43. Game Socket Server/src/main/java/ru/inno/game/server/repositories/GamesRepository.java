package ru.inno.game.server.repositories;

import ru.inno.game.server.models.Game;

/**
 * 07.08.2021
 * 40. GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface GamesRepository {
    void save(Game game);

    Game getById(Long gameId);

    void update(Game game);
}
