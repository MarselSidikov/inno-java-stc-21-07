package ru.inno.hard;

/**
 * 17.06.2021
 * 20. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface MailService {
    void sendMessage(String email, String text);
}
