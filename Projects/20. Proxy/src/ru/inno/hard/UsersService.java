package ru.inno.hard;

/**
 * 17.06.2021
 * 20. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersService {
    public void signUp(String email, String password) {
        // сохранение информации в базу данных
        System.out.println("Регистрация прошла успешно!");
    }

    public void signIn(String email, String password) {
        // поиск пользователя в базе данных, проверка пароля
        System.out.println("Вход прошел успешно!");
    }

    public void resetPassword(String email, String password) {
        // сохранения информации в базу данных
        System.out.println("Заявка на смену пароля отправлена");
    }
}
