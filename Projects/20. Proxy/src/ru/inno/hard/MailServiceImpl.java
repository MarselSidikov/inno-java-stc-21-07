package ru.inno.hard;

/**
 * 17.06.2021
 * 20. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MailServiceImpl implements MailService {

    @Override
    public void sendMessage(String email, String text) {
        initializeSmtp();
        System.out.println("Сообщение <" + text + "> отправлено на " + email);
    }

    private void initializeSmtp() {
        // подключение к почте
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }
    }
}
