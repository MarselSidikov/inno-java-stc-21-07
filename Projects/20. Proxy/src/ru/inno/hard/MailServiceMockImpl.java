package ru.inno.hard;

/**
 * 17.06.2021
 * 20. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class MailServiceMockImpl implements MailService {
    @Override
    public void sendMessage(String email, String text) {
        System.out.println("Вызов фейкового сервиса для отправки почты.");
    }
}
