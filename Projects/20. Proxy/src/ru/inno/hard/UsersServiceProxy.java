package ru.inno.hard;

import java.time.LocalDateTime;

/**
 * 17.06.2021
 * 20. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersServiceProxy extends UsersService {
    private AfterForUsersService after;

    public void setAfter(AfterForUsersService after) {
        this.after = after;
    }

    @Override
    public void signUp(String email, String password) {
        super.signUp(email, password);
        executeAfter(email, "Уважаемый пользователь, вы зарегистрированы!");
    }

    @Override
    public void signIn(String email, String password) {
        super.signIn(email, password);
        executeAfter(email, "Уважаемый пользователь, вы вошли в систему в " + LocalDateTime.now());
    }

    @Override
    public void resetPassword(String email, String password) {
        super.resetPassword(email, password);
        executeAfter(email, "Уважаемый пользователь, был изменен пароль в " + LocalDateTime.now());
    }

    private void executeAfter(String email, String password) {
        if (after != null) {
            after.execute(email, password);
        }
    }
}
