package ru.inno.lite;

/**
 * 17.06.2021
 * 20. Proxy
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */

public class Main {
    public static void main(String[] args) {
        DriverProxy marsel = new DriverProxy("Марсель");

        marsel.setBefore(() -> System.out.println(marsel.getName() + " собрался поехать!"));
        marsel.setInstead(() -> System.out.println("Куда-то он поехал..."));
        marsel.setAfter(() -> System.out.println("А не кукухой ли?"));

        Driver airat = new Driver("Айрат");
        Driver victor = new Driver("Виктор");

        Driver[] drivers = {airat, victor, marsel};

        for (int i = 0; i < drivers.length; i++) {
            drivers[i].drive();
        }
    }
}
