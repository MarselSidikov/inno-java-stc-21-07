package ru.inno;

/**
 * 17.06.2021
 * 21. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Certificate implements Document {

    private String text;
    private String title;

    public Certificate(String text, String title) {
        this.text = text;
        this.title = title;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public String getTitle() {
        return title;
    }
}
