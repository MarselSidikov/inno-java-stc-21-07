package ru.inno;

/**
 * 17.06.2021
 * 21. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface DocumentsGenerator {
    // фабричный метод, возвращает Document
    Document generate(String text);
}
