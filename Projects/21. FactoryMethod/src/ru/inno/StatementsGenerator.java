package ru.inno;

import java.time.LocalDate;

/**
 * 17.06.2021
 * 21. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class StatementsGenerator implements DocumentsGenerator {
    // реализация фабричного метода, возвращает конкретную реализацию документа
    @Override
    public Document generate(String text) {
        return new Statement("Заявление от " + LocalDate.now(), text);
    }
}
