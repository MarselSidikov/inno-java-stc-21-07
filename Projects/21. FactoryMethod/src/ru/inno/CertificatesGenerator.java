package ru.inno;

import java.time.LocalDate;

/**
 * 17.06.2021
 * 21. FactoryMethod
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class CertificatesGenerator implements DocumentsGenerator {
    @Override
    public Document generate(String text) {
        return new Certificate("Справка от " + LocalDate.now(), text);
    }
}
