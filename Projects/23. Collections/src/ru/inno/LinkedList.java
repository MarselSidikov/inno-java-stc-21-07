package ru.inno;

/**
 * 21.06.2021
 * 23. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList implements List {

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }
    }
    // ссылка на начало списка, на первый узел
    private Node first;
    // ссылка на последний элемент списка, на последний узел
    private Node last;

    private int size;

    @Override
    public int get(int index) {
        if (index >= 0 && index < size) {
            Node current = first;

            for (int i = 0; i < index; i++) {
                // отсчитываете index-штук узлов до нужного
                current = current.next;
            }
            // возвращаете его значение
            return current.value;
        }
        System.err.println("Index out of bounds");
        return -1;
    }

    @Override
    public void addFirst(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            add(element);
        } else {
            // если уже есть элементы в списке
            // следующий узел после нового - это первый узел списка
            newNode.next = first;
            // теперь новый узел - первый в списке
            first = newNode;
            size++;
        }
    }

//    @Override
//    public void add(int element) {
//        Node newNode = new Node(element);
//        // если первый узел отсутствует
//        // то новый узел и есть первый
//        if (first == null) {
//            first = newNode;
//        } else {
//            // если узлы уже есть, то нужно дойти до последнего, и добавить новый после него
//            Node current = first;
//            // идем по узлам, пока не дошли до последнего, последний узел, этот тот, у которого нет
//            // следующего
//            while (current.next != null) {
//                current = current.next;
//            }
//            // делаем новый узел следующим после последнего
//            current.next = newNode;
//        }
//        size++;
//    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        // если в списке еще нет элементов
        if (first == null) {
            // то первый и последний элемент - это новый
            first = newNode;
            last = newNode;
        } else {
            // теперь последний элемент списка ссылается на новый, следовательно новый встал в конец списка
            last.next = newNode;
            // теперь новый узел является последним
            last = newNode;
        }
        size++;
    }

    @Override
    public boolean contains(int element) {
        return false;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void remove(int element) {

    }

    @Override
    public Iterator iterator() {
        return null;
    }
}
