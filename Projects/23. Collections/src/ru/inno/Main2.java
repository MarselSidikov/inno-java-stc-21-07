package ru.inno;

/**
 * 21.06.2021
 * 23. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        List list = new LinkedList();
        list.add(10);
        list.add(15);
        list.add(11);
        list.add(17);
        list.add(12);

//        list.addFirst(777);
//        list.addFirst(888);
//        list.addFirst(999);

        System.out.println(list.get(0)); // 10
        System.out.println(list.get(3)); // 17
        System.out.println(list.get(4)); // 12
    }
}
