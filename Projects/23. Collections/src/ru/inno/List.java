package ru.inno;

/**
 * 21.06.2021
 * 23. Collections
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 *
 * Список это такая коллекция, где сохраняется порядок добавления элементов.
 * Если вы добавили элемент 6-м, то он и будет доступен под 6-м номеров
 */

public interface List extends Collection {

    /**
     * Возвращает элемент под заданным индексом
     * @param index индекс элемента
     * @return элемент
     */
    int get(int index);

    void addFirst(int element);
}
