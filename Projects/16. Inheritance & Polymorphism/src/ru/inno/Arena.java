package ru.inno;

/**
 * 10.06.2021
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Arena {
    // арена не знает, какие бегуны к ней пришли, ей важно, чтобы они все были людьми
    // и у них был метод run

    // такой код называется полиморфным
    // он может работать с объектами разных типов таким образом, будто они принадлежат одному типу
    public void start(Human[] humans) {
        for (int i = 0; i < humans.length; i++) {
            humans[i].run();
        }
    }
}
