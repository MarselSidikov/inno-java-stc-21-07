package ru.inno;

/**
 * 10.06.2021
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        Athlete airat = new Athlete("Айрат", 22, 5);
        Student student = new Student("Максим", 22, 4.9);
        Programmer programmer = new Programmer("Виктор", 24, 9);

        // человек делает 10 шагов
        // спортсмен делает 50 шагов
        // студент делает 5 шагов
        // программист делает 3 шага

        // восходящее преобразование
        Human h1 = airat;
        Human h2 = student;
        Human h3 = programmer;

        h1.run();
        h2.run();
        h3.run();

        h1.go();

    }
}
