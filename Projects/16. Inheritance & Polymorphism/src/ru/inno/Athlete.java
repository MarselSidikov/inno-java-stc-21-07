package ru.inno;

/**
 * 10.06.2021
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// Athlete - потомок класса Human, в нем содержится все, что есть в Human (кроме конструктора)
public class Athlete extends Human implements SalaryMan {

    private static final int MIN_RANK = 1;

    private int rank;

    public Athlete(String name, int age, int rank) {
        // вызов конструктора класса Human, потому что там уже написана определенная логика проверок и инициализаций
        super(name, age);
        if (rank >= MIN_RANK) {
            this.rank = rank;
        } else {
            this.rank = MIN_RANK;
        }
    }
    // убрали все, что 1 в 1 дублирует класс Human

    // но оставили метод run(), потому что он у нас работает не так, как в Human

    // метод, который в точности копирует сигнатуру метода в классе-предке (тип возвращаемого значения имя(формальные параметры))
    // но при этом имеет другую реализацию в классе-потомке называется ПЕРЕОПРЕДЕЛЕННЫМ
    public void run() {
        System.out.println(this.name + " побежал, сделал 50 шагов");
        this.stepsCount += 50;
    }

    public int getRank() {
        return rank;
    }

    public void getSalary() {
        System.out.println("Набегал на зарплату:)");
    }
}
