package ru.inno;

/**
 * 10.06.2021
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public abstract class Human {
    private static final int MIN_AGE = 0;
    // protected - модификатор доступа, который определяет уровень доступа "защищенный" -> член класса доступен только потомкам
    protected String name;
    private int age;

    protected int stepsCount;

    public Human(String name, int age) {
        this.name = name;
        if (age >= MIN_AGE) {
            this.age = age;
        } else {
            this.age = MIN_AGE;
        }
        this.stepsCount = 0;
    }

    public void go() {
        System.out.println("Человек" + name + " пошел, сделал один шаг");
        this.stepsCount++;
    }

    public abstract void run();

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public int getStepsCount() {
        return stepsCount;
    }
}
