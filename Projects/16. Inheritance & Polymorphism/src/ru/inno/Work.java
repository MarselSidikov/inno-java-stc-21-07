package ru.inno;

/**
 * 10.06.2021
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Work {
    public void doWork(WorkMan[] workMen) {
        for (int i = 0; i < workMen.length; i++) {
            workMen[i].work();
        }
    }

    public void giveSalary(SalaryMan[] salaryMen) {
        for (int i = 0; i < salaryMen.length; i++) {
            salaryMen[i].getSalary();
        }
    }
}
