package ru.inno;

/**
 * 10.06.2021
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Student extends Human implements WorkMan {
    private double averageMark;

    public Student(String name, int age, double averageMark) {
        super(name, age);
        this.averageMark = averageMark;
    }

    public double getAverageMark() {
        return averageMark;
    }
    // переопределенный метод
    public void run() {
        System.out.println("Студент " + name + " побежал и сделал 5 шагов!");
        this.stepsCount+= 5;
    }

    public void work() {
        System.out.println("Студент пошел на работу");
    }
}
