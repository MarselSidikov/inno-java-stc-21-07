package ru.inno;

public class Main {

    public static void main(String[] args) {
//	    Human marsel = new Human("Марсель", 27);
//	    marsel.go();
//	    marsel.run();
//        System.out.println(marsel.getStepsCount());

        Athlete airat = new Athlete("Айрат", 22, 5);
        airat.go();
        airat.run();
        System.out.println(airat.getStepsCount());

        Student student = new Student("Максим", 22, 4.9);
        student.run();
    }
}
