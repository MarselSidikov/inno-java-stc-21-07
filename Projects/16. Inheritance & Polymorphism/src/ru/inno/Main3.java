package ru.inno;

/**
 * 10.06.2021
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        Arena arena = new Arena();

//        Human human = new Human("Человек", 25);

        Athlete airat = new Athlete("Айрат", 22, 5);
        Student student = new Student("Максим", 22, 4.9);
        Programmer programmer = new Programmer("Виктор", 24, 9);

        // создаем массив людей, хотя на самом деле этот массив содержит объекты разных типов
        Human[] runners = {airat, student, programmer};

        arena.start(runners);
    }
}
