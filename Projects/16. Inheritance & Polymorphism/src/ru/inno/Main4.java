package ru.inno;

/**
 * 10.06.2021
 * 16. Inheritance & Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main4 {
    public static void main(String[] args) {
        WorkMan w1 = new Programmer("Виктор", 24, 9);
        WorkMan w2 = new Student("Максим", 22, 2);
//        WorkMan w3 = new Athlete("Айрат", 22, 5);

        Work work = new Work();
        work.doWork(new WorkMan[]{w1, w2});

        work.giveSalary(new SalaryMan[]{new Athlete("Айрат", 22, 5),
                new Programmer("Виктор", 24, 9)});
    }
}
